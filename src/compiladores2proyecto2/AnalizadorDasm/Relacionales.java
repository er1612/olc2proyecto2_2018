/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import static compiladores2proyecto2.AnalizadorDasm.Aritmeticas.getRespuesta;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.aux;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.errorAux;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class Relacionales {
    
    //-----------------------------------------------------
    //Operaciones relacionales
    //-----------------------------------------------------
    
    public static int getMenor(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = 0;
        
        if(num1<num2)
        {
            resultado = 1;
        }
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getMenorIgual(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = 0;
        
        if(num1<=num2)
        {
            resultado = 1;
        }
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getMayor(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = 0;
        
        if(num1>num2)
        {
            resultado = 1;
        }
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getMayorIgual(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = 0;
        
        if(num1>=num2)
        {
            resultado = 1;
        }
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getEqz(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = 0;
        
        if(num1==0)
        {
            resultado = 1;
        }
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
}
