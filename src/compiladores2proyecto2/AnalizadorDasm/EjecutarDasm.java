/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

/**
 *
 * @author rodolfo
 */
public class EjecutarDasm extends Thread{
    
    public static VistaMemoria vistaMem = new VistaMemoria();
    public static Thread hilo;
    public static LinkedList<PosMem> stack = new LinkedList<PosMem>();
    public static LinkedList<PosMem> aux = new LinkedList<PosMem>();
    public static LinkedList<PosMem> heap = new LinkedList<PosMem>();
    public static LinkedList<Proc> procs = new LinkedList<Proc>();
    
    private static Nodo raiz = null;
    private static Proc principal = null;

    public static Nodo getRaiz() 
    {
        return raiz;
    }

    public static void setRaiz(Nodo raiz) 
    {
        EjecutarDasm.raiz = raiz;
    }
    
    public static void Ejecutar()
    {
        hilo = new Thread(){
            @Override
            public void run(){
                ejecutarDasm();
            }
          };
        
        hilo.start();
    }
    
    private static void ejecutarDasm()
    {
        stack.clear();
        aux.clear();
        heap.clear();
        procs.clear();
        principal = null;

        vistaMem.setVisible(true);

        buscarFunciones(raiz);

        if(principal!=null)
        {
            ejecutarProcedimiento(principal);
        }
        
        vistaMem.actualizarEstructuras();
    }
    
    
    private static void buscarFunciones(Nodo nodo)
    {
        for(Nodo pro : nodo.getNodos())
        {
            Nodo nodoSentencias = pro.getNodo(0);
            Proc proc = new Proc();
            proc.setId(pro.getToken());
            proc.setNodoSentencias(nodoSentencias);
            
            for(int x=0; x<nodoSentencias.getNodos().size(); x++)
            {
                Nodo sentencia = nodoSentencias.getNodo(x);
                
                if(sentencia.getNombre().equals("etiqueta"))
                {
                    Etiqueta etq = new Etiqueta();
                    etq.setId(sentencia.getToken());
                    etq.setIndice(x);
                    
                    proc.addEtqueta(etq);
                }
            }
            
            if(proc.getId().equalsIgnoreCase("principal"))
            {
                principal = proc;
            }
            else
            {
                procs.add(proc);
            }
        }
        
        
    }
    
    public static int ejecutarProcedimiento(Proc proc)
    {
        return Sentencias(proc);
    }
    
    private static int Sentencias(Proc proc)
    {
        Nodo nodo = proc.getNodoSentencias();
        
        int cantSentencias = nodo.getNodos().size();
        
        for(int x=0; x<cantSentencias; x++)
        {
            Nodo sentencia = nodo.getNodo(x);

            String nombre = sentencia.getNombre();
            int resultado = 0;
            
            switch(nombre)
            {
                case "entero":
                    resultado = Aritmeticas.numeroEntero(sentencia);
                    break;
                case "decimal":
                    resultado = Aritmeticas.numeroDecimal(sentencia);
                    break;
                case "mas":
                    resultado = Aritmeticas.getSuma(sentencia);
                    break;
                case "menos":
                    resultado = Aritmeticas.getResta(sentencia);
                    break;
                case "por":
                    resultado = Aritmeticas.getMultiplicacion(sentencia);
                    break;
                case "dividido":
                    resultado = Aritmeticas.getDivision(sentencia);
                    break;
                case "modulo":
                    resultado = Aritmeticas.getModulo(sentencia);
                    break;
                case "menor":
                    resultado = Relacionales.getMenor(sentencia);
                    break;
                case "mayor":
                    resultado = Relacionales.getMayor(sentencia);
                    break;
                case "menor_igual":
                    resultado = Relacionales.getMenorIgual(sentencia);
                    break;
                case "mayor_igual":
                    resultado = Relacionales.getMayorIgual(sentencia);
                    break;
                case "salto":
                    x = Saltos.saltoIncondicional(sentencia, proc);
                    break;
                case "salto_condicional":
                    resultado = Saltos.saltoCondicional(sentencia, proc);
                    if(resultado == - 100)
                    {
                        resultado = 100;
                    }
                    else if(resultado != -1)
                    {
                        x = resultado;
                    }
                    break;
                case "llamado":
                    resultado = Saltos.llamadoProcedimiento(sentencia);
                    break;
                case "set_local":
                    StackHeap.setLocal(sentencia);
                    break;
                case "set_global":
                    StackHeap.setGlobal(sentencia);
                    break;
                case "get_local":
                    StackHeap.getLocal(sentencia);
                    break;
                case "get_global":
                    StackHeap.getGlobal(sentencia);
                    break;
                case "imprimir_caracter":
                    Imprimir.imprimirCaracter();
                    break;
                case "imprimir_entero":
                    Imprimir.imprimirEntero();
                    break;
                case "imprimir_decimal":
                    Imprimir.imprimirDecimal();
                    break;
                case "print":
                    Imprimir.ejecutarImprimir();
                    break;
                case "eqz":
                    Relacionales.getEqz(sentencia);
                    break;
                case "ret":
                    return 0;
            }
            
            if(resultado==100)
            {
                return 100;
            }
            
            if(Entorno.esDepuracion)
            {
                vistaMem.actualizarEstructuras();
                Entorno.pintarLinea(sentencia.getArchivo(), sentencia.getFila());
            }
        }
        return 0;
    }
    
    public static void errorAux(Nodo nodo)
    {
        int fila = nodo.getFila();
        int columna = nodo.getColumna();
        
        RegistroError re = new RegistroError();
        re.setArchivo(nodo.getArchivo());
        re.setFila(fila);
        re.setTipo("Semantico");
        re.setColumna(columna);
        re.setDescripcion("Valores insuficientes en la pila auxiliar.");
        
        Entorno.addError(re);
    }
}
