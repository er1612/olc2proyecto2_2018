/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.aux;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class Imprimir {
    
    public static int imprimirCaracter()
    {
        PosMem mem = new PosMem();
        mem.setValor(100);
        mem.setTamano(0);
        
        aux.addLast(mem);
        
        return 0;
    }
    
    public static int imprimirEntero()
    {
        PosMem mem = new PosMem();
        mem.setValor(200);
        mem.setTamano(0);
        
        aux.addLast(mem);
        
        return 0;
    }
    
    public static int imprimirDecimal()
    {
        PosMem mem = new PosMem();
        mem.setValor(300);
        mem.setTamano(0);
        
        aux.addLast(mem);
        
        return 0;
    }
    
    public static void ejecutarImprimir()
    {
        Double valorOriginal = aux.getLast().getValor();
        aux.removeLast();
        
        Double parametroOriginal = aux.getLast().getValor();
        int parametro = parametroOriginal.intValue();
        aux.removeLast();
        
        switch(parametro)
        {
            case 100:
                int valorCaracter = valorOriginal.intValue();
                Entorno.getVentanaPrincipal().textoConsola.append(String.valueOf((char)valorCaracter));
                break;
            case 200:
                int valorEntero = valorOriginal.intValue();
                Entorno.getVentanaPrincipal().textoConsola.append(String.valueOf(valorEntero));
                break;
            case 300:
                Entorno.getVentanaPrincipal().textoConsola.append(String.valueOf(valorOriginal));
                break;
        }
    }
}
