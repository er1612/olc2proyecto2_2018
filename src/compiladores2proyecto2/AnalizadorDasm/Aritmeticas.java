/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.aux;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.errorAux;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Aritmeticas {
    
    //-----------------------------------------------------
    //Operaciones aritmeticas
    //-----------------------------------------------------
    
    public static int numeroEntero(Nodo nodo)
    {
        double num = Integer.parseInt(nodo.getToken());
        
        PosMem mem = new PosMem();
        mem.setValor(num);
        mem.setTamano(4);
        
        aux.addLast(mem);
        
        return 0;
    }
    
     public static int numeroDecimal(Nodo nodo)
    {
        double num = Double.parseDouble(nodo.getToken());
        
        PosMem mem = new PosMem();
        mem.setValor(num);
        mem.setTamano(8);
        
        aux.addLast(mem);
        
        return 0;
    }
    
    public static int getSuma(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = num1 + num2;
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getResta(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = num1 - num2;
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getMultiplicacion(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        double resultado = num1 * num2;
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getDivision(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        if(num2==0)
        {
            errorCero(nodo);
            return 100;
        }
        
        double resultado = num1 / num2;
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getModulo(Nodo nodo)
    {
        //Valor 1 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num2 = aux.getLast().getValor();
        aux.removeLast();
        
        //Valor 2 -------------------------------
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return 100;
        }
        
        double num1 = aux.getLast().getValor();
        aux.removeLast();
        
        if(num2==0)
        {
            errorCero(nodo);
            return 100;
        }
        
        double resultado = num1 % num2;
        
        PosMem mem = new PosMem();
        mem.setValor(resultado);
        mem.setTamano(4);
        
        int respuesta = getRespuesta(mem);
        
        aux.addLast(mem);
        
        return respuesta;
    }
    
    public static int getRespuesta(PosMem mem)
    {
        return 0;
    }
    
    public static void errorCero(Nodo nodo)
    {
        int fila = nodo.getFila();
        int columna = nodo.getColumna();
        
        RegistroError re = new RegistroError();
        re.setArchivo(nodo.getArchivo());
        re.setFila(fila);
        re.setTipo("Semantico");
        re.setColumna(columna);
        re.setDescripcion("No se puede dividir entre cero (0).");
        
        Entorno.addError(re);
    }
    
}
