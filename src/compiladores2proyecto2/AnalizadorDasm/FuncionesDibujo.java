/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.aux;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.heap;
import compiladores2proyecto2.AnalizadorDracoScript.EjecutarDraco;
import compiladores2proyecto2.Dibujo;

/**
 *
 * @author rodolfo
 */
public class FuncionesDibujo {
    
    public static void generarPunto()
    {
        System.out.println("Punto");
        Double val4 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val3 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val2 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val1 = aux.getLast().getValor();
        aux.removeLast();
        
        int diametro = val4.intValue();
        String hex = "#" + Integer.toHexString(val3.intValue());
        int y = val2.intValue();
        int x = val1.intValue();
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setX(x);
        dibujo.setY(y);
        dibujo.setAlto(diametro);
        dibujo.setAncho(diametro);
        dibujo.setColor(hex);
        dibujo.setForma("punto");
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void generarCuadrado()
    {
        System.out.println("Cuadrado");
        Double val1 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val2 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val3 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val4 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val5 = aux.getLast().getValor();
        aux.removeLast();
        
        int alto = val1.intValue();
        int ancho = val2.intValue();
        String hex = "#" + Integer.toHexString(val3.intValue());
        int y = val4.intValue();
        int x = val5.intValue();
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setX(x);
        dibujo.setY(y);
        dibujo.setAlto(alto);
        dibujo.setAncho(ancho);
        dibujo.setColor(hex);
        dibujo.setForma("rectangulo");
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void generarOvalo()
    {
        System.out.println("Ovalo");
        Double val1 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val2 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val3 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val4 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val5 = aux.getLast().getValor();
        aux.removeLast();
        
        int alto = val1.intValue();
        int ancho = val2.intValue();
        String hex = "#" + Integer.toHexString(val3.intValue());
        int y = val4.intValue();
        int x = val5.intValue();
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setX(x);
        dibujo.setY(y);
        dibujo.setAlto(alto);
        dibujo.setAncho(ancho);
        dibujo.setColor(hex);
        dibujo.setForma("circulo");
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void generarCadena()
    {
        System.out.println("Cadena");
        Double val1 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val2 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val3 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val4 = aux.getLast().getValor();
        aux.removeLast();
        
        String cadena  = "";
        
        System.out.println("Valor inicial " + val1);
        
        for(int c = val1.intValue(); c<heap.size(); c++)
        {
            if(c==0)
            {
                break;
            }
            Double caracter = heap.get(c).getValor();
            cadena += (char) caracter.intValue();
        }
        
        System.out.println(cadena);
        
        String hex = "#" + Integer.toHexString(val2.intValue());
        int y = val3.intValue();
        int x = val4.intValue();
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setX(x);
        dibujo.setY(y);
        dibujo.setColor(hex);
        dibujo.setTexto(cadena);
        dibujo.setForma("cadena");
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void generarLinea()
    {
        System.out.println("Linea");
        Double val1 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val2 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val3 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val4 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val5 = aux.getLast().getValor();
        aux.removeLast();
        
        Double val6 = aux.getLast().getValor();
        aux.removeLast();
        
        int ancho = val1.intValue();
        String hex = "#" + Integer.toHexString(val2.intValue());
        int yf = val3.intValue();
        int xf = val4.intValue();
        int y = val5.intValue();
        int x = val6.intValue();
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setX(x);
        dibujo.setY(y);
        dibujo.setXf(xf);
        dibujo.setYf(yf);
        dibujo.setAncho(ancho);
        dibujo.setColor(hex);
        dibujo.setForma("linea");
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    
}
