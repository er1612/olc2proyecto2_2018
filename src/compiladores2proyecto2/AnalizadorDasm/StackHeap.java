/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.aux;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.stack;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.heap;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.vistaMem;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class StackHeap {
    
    public static int getLocal(Nodo nodo)
    {
        int i = getIndice(nodo.getNodo(0));
        
        if(i<0)
        {
            return -1;
        }
        
        double valor = stack.get(i).getValor();
        
        PosMem posmem = new PosMem();
        posmem.setValor(valor);
        aux.addLast(posmem);
        
        return 0;
    }
    
    public static int setLocal(Nodo nodo)
    {
        int i = getIndice(nodo.getNodo(0));
        
        if(i<0)
        {
            return -1;
        }
        
        while(i+1>stack.size())
        {
            PosMem posmem = new PosMem();
            stack.addLast(posmem);
        }
        
        double valor = aux.getLast().getValor();
        aux.removeLast();
        stack.get(i).setValor(valor);
        
        return 0;
    }
    
    public static int getGlobal(Nodo nodo)
    {
        int i = getIndice(nodo.getNodo(0));
        
        if(i<0)
        {
            return -1;
        }
        
        double valor = heap.get(i).getValor();
        
        PosMem posmem = new PosMem();
        posmem.setValor(valor);
        aux.addLast(posmem);
        
        return 0;
    }
    
    public static int setGlobal(Nodo nodo)
    {
        int i = getIndice(nodo.getNodo(0));
        
        if(i<0)
        {
            return -1;
        }
        
        while(i+1>heap.size())
        {
            PosMem posmem = new PosMem();
            heap.addLast(posmem);
        }
        
        double valor = aux.getLast().getValor();
        aux.removeLast();
        heap.get(i).setValor(valor);
        
        return 0;
    }
    
    private static int getIndice(Nodo nodo)
    {
        if(nodo.getTipo().equalsIgnoreCase("entero"))
        {
            Double indice = Double.parseDouble(nodo.getToken());
            int i = indice.intValue();
            return i;
        }else if(nodo.getTipo().equalsIgnoreCase("calc"))
        {
            Double indice = aux.getLast().getValor();
            aux.removeLast();
            int i = indice.intValue();
            return i;
        }
        
        return -1;
    }
    
}
