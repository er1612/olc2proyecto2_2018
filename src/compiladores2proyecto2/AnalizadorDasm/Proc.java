/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import compiladores2proyecto2.Nodo;
import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class Proc {
    
    private String id;
    private Nodo nodoSentencias;
    private LinkedList<Etiqueta> etiquetas = new LinkedList<Etiqueta>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Nodo getNodoSentencias() {
        return nodoSentencias;
    }

    public void setNodoSentencias(Nodo nodoSentencias) {
        this.nodoSentencias = nodoSentencias;
    }
    
    public int getIndiceEtiqueta(String id)
    {
        for(Etiqueta salto : etiquetas)
        {
            if(id.equals(salto.getId()))
            {
                return salto.getIndice();
            }
        }
        return 0;
    }
    
    public void addEtqueta(Etiqueta etq)
    {
        etiquetas.addLast(etq);
    }
    
}
