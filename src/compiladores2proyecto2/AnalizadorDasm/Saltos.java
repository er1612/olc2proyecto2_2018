/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDasm;

import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.aux;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.ejecutarProcedimiento;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.errorAux;
import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.procs;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class Saltos {
    
    public static int saltoIncondicional(Nodo nodo, Proc proc)
    {
        String id = nodo.getToken();
        return proc.getIndiceEtiqueta(id);
    }
    
    public static int saltoCondicional(Nodo nodo, Proc proc)
    {
        if(aux.isEmpty())
        {
            errorAux(nodo);
            return -100;
        }
        
        double num = aux.getLast().getValor();
        aux.removeLast();
        
        if(num==0)
        {
            String id = nodo.getToken();
            return proc.getIndiceEtiqueta(id);
        }
        
        return -1;
    }
    
    public static int llamadoProcedimiento(Nodo nodo)
    {
        String id = nodo.getToken();
        
        if(id.equals("Point"))
        {
            FuncionesDibujo.generarPunto();
        }
        if(id.equals("Quadrate"))
        {
            FuncionesDibujo.generarCuadrado();
        }
        if(id.equals("Oval"))
        {
            FuncionesDibujo.generarOvalo();
        }
        if(id.equals("String"))
        {
            FuncionesDibujo.generarCadena();
        }
        if(id.equals("Line"))
        {
            FuncionesDibujo.generarLinea();
        }
        else
        {
            for(Proc proc : procs)
            {
                if(id.equals(proc.getId()))
                {
                    ejecutarProcedimiento(proc);
                    break;
                }
            }
        }
        
        return 0;
    }
}
