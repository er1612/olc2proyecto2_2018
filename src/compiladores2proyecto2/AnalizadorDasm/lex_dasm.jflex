package compiladores2proyecto2.AnalizadorDasm;
import java_cup.runtime.Symbol;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.Entorno;
import java.awt.Color;
import compiladores2proyecto2.RegistroError;
%%
%cupsym sim_dasm
%class lex_dasm
%cup
%public
%unicode
%line
%char
%column
%ignorecase

%{
    public static String archivo = "";

	public void PrintToken(String str){
		System.out.println(str);
	}
%}

digito				=	[0-9]
entero 				=	("-")?([0-9])+
ndecimal			=	("-")?([0-9])+("."([0-9])+)
comentario 			=	"//" (.)* "\n"
comentario_multi	=	"/*" ([^])* "*/"
identificador		=	([a-zA-Z]|"_")([a-zA-Z]|[0-9]|"_")*

%%
{entero}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#721783"));
	return new Symbol(sim_dasm.entero, yycolumn,yyline,new String(yytext()));
}

{ndecimal}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#721783"));
	return new Symbol(sim_dasm.ndecimal, yycolumn,yyline,new String(yytext()));
}

//Operadores Aritmeticos---------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"Add"				{return new Symbol(sim_dasm.mas, yycolumn,yyline,new String(yytext()));}
"Diff"				{return new Symbol(sim_dasm.menos, yycolumn,yyline,new String(yytext()));}
"Mult"				{return new Symbol(sim_dasm.por, yycolumn,yyline,new String(yytext()));}
"Div"				{return new Symbol(sim_dasm.dividido, yycolumn,yyline,new String(yytext()));}
"Mod"				{return new Symbol(sim_dasm.modulo, yycolumn,yyline,new String(yytext()));}
//Operadores Realcionales--------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"Gt"				{return new Symbol(sim_dasm.mayor, yycolumn,yyline,new String(yytext()));}
"Lt"				{return new Symbol(sim_dasm.menor, yycolumn,yyline,new String(yytext()));}
"Gte"				{return new Symbol(sim_dasm.mayor_igual, yycolumn,yyline,new String(yytext()));}
"Lte"				{return new Symbol(sim_dasm.menor_igual, yycolumn,yyline,new String(yytext()));}
"Eqz"				{return new Symbol(sim_dasm.igual_cero, yycolumn,yyline,new String(yytext()));}
//Operadores Logicos-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"And"				{return new Symbol(sim_dasm.and, yycolumn,yyline,new String(yytext()));}
"Or"				{return new Symbol(sim_dasm.or, yycolumn,yyline,new String(yytext()));}
"Not"				{return new Symbol(sim_dasm.not, yycolumn,yyline,new String(yytext()));}
"Br"				{return new Symbol(sim_dasm.salto, yycolumn,yyline,new String(yytext()));}
"Br_if"				{return new Symbol(sim_dasm.salto_condicional, yycolumn,yyline,new String(yytext()));}
//Palabras reservadas------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"Get_local"			{return new Symbol(sim_dasm.getLocal, yycolumn,yyline,new String(yytext()));}
"Get_global"		{return new Symbol(sim_dasm.getGlobal, yycolumn,yyline,new String(yytext()));}
"Set_local"			{return new Symbol(sim_dasm.setLocal, yycolumn,yyline,new String(yytext()));}
"Set_global"		{return new Symbol(sim_dasm.setGlobal, yycolumn,yyline,new String(yytext()));}
"Tee_local"			{return new Symbol(sim_dasm.teeLocal, yycolumn,yyline,new String(yytext()));}
"Tee_global"		{return new Symbol(sim_dasm.teeGlobal, yycolumn,yyline,new String(yytext()));}
"Function"			{return new Symbol(sim_dasm.rfunction, yycolumn,yyline,new String(yytext()));}
"End"				{return new Symbol(sim_dasm.rend, yycolumn,yyline,new String(yytext()));}
"Call"				{return new Symbol(sim_dasm.rcall, yycolumn,yyline,new String(yytext()));}
"calc"				{return new Symbol(sim_dasm.calc, yycolumn,yyline,new String(yytext()));}
"ret"				{return new Symbol(sim_dasm.ret, yycolumn,yyline,new String(yytext()));}
//Palabras reservadas------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"Print"				{return new Symbol(sim_dasm.rprint, yycolumn,yyline,new String(yytext()));}
//Palabras reservadas------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"$"					{return new Symbol(sim_dasm.dolar, yycolumn,yyline,new String(yytext()));}
"\"%c\""			{return new Symbol(sim_dasm.imprimir_caracter, yycolumn,yyline,new String(yytext()));}
"\"%d\""			{return new Symbol(sim_dasm.imprimir_entero, yycolumn,yyline,new String(yytext()));}
"\"%f\""			{return new Symbol(sim_dasm.imprimir_decimal, yycolumn,yyline,new String(yytext()));}

{identificador}		{return new Symbol(sim_dasm.identificador, yycolumn,yyline,new String(yytext()));}

{comentario}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#737373"));
}

{comentario_multi}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#737373"));
}

[ \t\r\f\n]+		{}

.
{
	RegistroError err = new RegistroError();
    err.setArchivo(Entorno.getArchivo());
    err.setFila(yyline);
    err.setColumna(yycolumn);
    err.setTipo("Lexico");
    err.setDescripcion("No se reconoce el simbolo " + yytext() + " .");
    Entorno.addError(err);
}

