/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class Dato {
    private String valor = "";
    private String tipo = "";
    private String etqV = "";
    private String etqF = "";
    

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public void addEtqFalso(String etq){
        this.etqF = etq;
    }
    
    public void addEtqVerdadero(String etq){
        this.etqV = etq;
    }

    public String getEtqV() {
        return etqV;
    }

    public String getEtqF() {
        return etqF;
    }
    
    public void concat(String cad)
    {
        this.valor += cad + "\n";
    }
}
