/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class CicloFor {
    
    public static void ejecutarFor(Nodo nodo)
    {
        Nodo nodoInicializacion = nodo.getNodo(0);
        Nodo nodoExpresion = nodo.getNodo(1);
        Nodo nodoActualizacion = nodo.getNodo(2);
        Nodo nodoSentencias = nodo.getNodo(3);
        
        //------------------------------------------------------
        //Ejecutando la inicializacion del ciclo for
        //------------------------------------------------------
        int cantidadVariables1 = EjecutarDraco.ambito.size();
        EjecutarDraco.ejecutarSentencias(nodoInicializacion);
        
        //Para primera iteracion
        Dato expresion = Expresiones.nodoExpresion(nodoExpresion);
        if(!expresion.getTipo().equalsIgnoreCase("booleano"))
        {
            EjecutarDraco.reportarErrorBooleano(nodoExpresion);
            return;
        }
        
        while(expresion.getValor().equalsIgnoreCase("1"))
        {
            //Ambito
            int cantidadVariables2 = EjecutarDraco.ambito.size();
            
            int resultado = EjecutarDraco.ejecutarSentencias(nodoSentencias);
            EjecutarDraco.restaurarAmbito(cantidadVariables2);
            
            if(resultado == 2 )
            {
                EjecutarDraco.restaurarAmbito(cantidadVariables1);
                break;
            }
            
            //------------------------------------------------------
            //Ejecutando Actualizacion
            //------------------------------------------------------
            EjecutarDraco.ejecutarSentencias(nodoActualizacion);
            
            //Para n iteracion
            expresion = Expresiones.nodoExpresion(nodoExpresion);
        
            //Comprobando que el dato sea booleano
            if(!expresion.getTipo().equalsIgnoreCase("booleano"))
            {
                EjecutarDraco.reportarErrorBooleano(nodoExpresion);
                return;
            }
        }
        
        EjecutarDraco.restaurarAmbito(cantidadVariables1);
    }
    
}
