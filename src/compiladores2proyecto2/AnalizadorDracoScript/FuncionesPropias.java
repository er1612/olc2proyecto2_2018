/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Dibujo;
import java.awt.Color;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.RegistroError;
import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rodolfo
 */
public class FuncionesPropias {
    
    public static void funcionPrint(Nodo nodo)
    {
        Nodo expresion = nodo.getNodo(0);
        
        Dato dato = Expresiones.nodoExpresion(expresion);
        
        if(dato.getTipo().equalsIgnoreCase("nulo"))
        {
            errorNulo(nodo);
            return;
        }
        
        Entorno.getVentanaPrincipal().textoConsola.append(dato.getValor() + "\n");
    }
    
    public static void dibujarPunto(Nodo nodo)
    {
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        
        if(cantidadParametros != 4)
        {
            errorCantidadParametros(nodo, 4, cantidadParametros);
        }
        
        Dato parametro0 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(0));
        Dato parametro1 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(1));
        Dato parametro2 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(2));
        Dato parametro3 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(3));
        
        //Comprobando tipo de parametros
        if(!parametro0.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(0), "entero", parametro0.getTipo());
            return;
        }
        
        if(!parametro1.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(1), "entero", parametro1.getTipo());
            return;
        }
        
        if(!parametro2.getTipo().equalsIgnoreCase("cadena"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(2), "cadena", parametro2.getTipo());
            return;
        }
        
        if(!parametro3.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(3), "entero", parametro3.getTipo());
            return;
        }
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setForma("punto");
        
        dibujo.setX(Integer.parseInt(parametro0.getValor()));
        dibujo.setY(Integer.parseInt(parametro1.getValor()));
        dibujo.setColor(parametro2.getValor());
        dibujo.setAncho(Integer.parseInt(parametro3.getValor()));
        dibujo.setAlto(Integer.parseInt(parametro3.getValor()));
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void dibujarRectangulo(Nodo nodo)
    {
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        
        if(cantidadParametros != 5)
        {
            errorCantidadParametros(nodo, 5, cantidadParametros);
        }
        
        Dato parametro0 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(0));
        Dato parametro1 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(1));
        Dato parametro2 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(2));
        Dato parametro3 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(3));
        Dato parametro4 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(4));
        
        //Comprobando tipo de parametros
        if(!parametro0.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(0), "entero", parametro0.getTipo());
            return;
        }
        
        if(!parametro1.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(1), "entero", parametro1.getTipo());
            return;
        }
        
        if(!parametro2.getTipo().equalsIgnoreCase("cadena"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(2), "cadena", parametro2.getTipo());
            return;
        }
        
        if(!parametro3.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(3), "entero", parametro3.getTipo());
            return;
        }
        
        if(!parametro4.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(4), "entero", parametro4.getTipo());
            return;
        }
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setForma("rectangulo");
        
        dibujo.setX(Integer.parseInt(parametro0.getValor()));
        dibujo.setY(Integer.parseInt(parametro1.getValor()));
        dibujo.setColor(parametro2.getValor());
        dibujo.setAncho(Integer.parseInt(parametro3.getValor()));
        dibujo.setAlto(Integer.parseInt(parametro4.getValor()));
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void dibujarCirculo(Nodo nodo)
    {
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        
        if(cantidadParametros != 5)
        {
            errorCantidadParametros(nodo, 5, cantidadParametros);
        }
        
        Dato parametro0 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(0));
        Dato parametro1 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(1));
        Dato parametro2 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(2));
        Dato parametro3 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(3));
        Dato parametro4 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(4));
        
        //Comprobando tipo de parametros
        if(!parametro0.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(0), "entero", parametro0.getTipo());
            return;
        }
        
        if(!parametro1.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(1), "entero", parametro1.getTipo());
            return;
        }
        
        if(!parametro2.getTipo().equalsIgnoreCase("cadena"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(2), "cadena", parametro2.getTipo());
            return;
        }
        
        if(!parametro3.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(3), "entero", parametro3.getTipo());
            return;
        }
        
        if(!parametro4.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(4), "entero", parametro4.getTipo());
            return;
        }
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setForma("circulo");
        
        dibujo.setX(Integer.parseInt(parametro0.getValor()));
        dibujo.setY(Integer.parseInt(parametro1.getValor()));
        dibujo.setColor(parametro2.getValor());
        dibujo.setAncho(Integer.parseInt(parametro3.getValor()));
        dibujo.setAlto(Integer.parseInt(parametro4.getValor()));
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void dibujarCadena(Nodo nodo)
    {
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        
        if(cantidadParametros != 4)
        {
            errorCantidadParametros(nodo, 4, cantidadParametros);
        }
        
        Dato parametro0 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(0));
        Dato parametro1 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(1));
        Dato parametro2 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(2));
        Dato parametro3 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(3));
        
        //Comprobando tipo de parametros
        if(!parametro0.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(0), "entero", parametro0.getTipo());
            return;
        }
        
        if(!parametro1.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(1), "entero", parametro1.getTipo());
            return;
        }
        
        if(!parametro2.getTipo().equalsIgnoreCase("cadena"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(2), "cadena", parametro2.getTipo());
            return;
        }
        
        if(!parametro3.getTipo().equalsIgnoreCase("cadena"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(3), "cadena", parametro3.getTipo());
            return;
        }
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setForma("cadena");
        
        dibujo.setX(Integer.parseInt(parametro0.getValor()));
        dibujo.setY(Integer.parseInt(parametro1.getValor()));
        dibujo.setColor(parametro2.getValor());
        dibujo.setTexto(parametro3.getValor());
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void dibujarLinea(Nodo nodo)
    {
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        
        if(cantidadParametros != 6)
        {
            errorCantidadParametros(nodo, 6, cantidadParametros);
        }
        
        Dato parametro0 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(0));
        Dato parametro1 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(1));
        Dato parametro2 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(2));
        Dato parametro3 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(3));
        Dato parametro4 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(4));
        Dato parametro5 = Expresiones.nodoExpresion(nodo.getNodo(0).getNodo(5));
        
        //Comprobando tipo de parametros
        if(!parametro0.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(0), "entero", parametro0.getTipo());
            return;
        }
        
        if(!parametro1.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(1), "entero", parametro1.getTipo());
            return;
        }
        
        if(!parametro2.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(2), "entero", parametro2.getTipo());
            return;
        }
        if(!parametro3.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(3), "entero", parametro3.getTipo());
            return;
        }
        
        if(!parametro4.getTipo().equalsIgnoreCase("cadena"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(4), "cadena", parametro4.getTipo());
            return;
        }
        
        if(!parametro5.getTipo().equalsIgnoreCase("entero"))
        {
            errorTipoParametro(nodo.getNodo(0).getNodo(5), "entero", parametro5.getTipo());
            return;
        }
        
        Dibujo dibujo = new Dibujo();
        
        dibujo.setForma("linea");
        
        dibujo.setX(Integer.parseInt(parametro0.getValor()));
        dibujo.setY(Integer.parseInt(parametro1.getValor()));
        dibujo.setXf(Integer.parseInt(parametro2.getValor()));
        dibujo.setYf(Integer.parseInt(parametro3.getValor()));
        dibujo.setColor(parametro4.getValor());
        dibujo.setAncho(Integer.parseInt(parametro5.getValor()));
        
        EjecutarDraco.dibujos.add(dibujo);
        EjecutarDraco.imagenDraco.actualizarImagen();
    }
    
    public static void errorNulo(Nodo nodo)
    {
        RegistroError err = new RegistroError();
        err.setArchivo(nodo.getArchivo());
        err.setFila(nodo.getFila());
        err.setColumna(nodo.getColumna());
        err.setTipo("Semantico");
        err.setDescripcion("No se puede imprimir un valor nulo.");
        Entorno.addError(err);
    }
    
    public static void errorCantidadParametros(Nodo nodo, int cantidadEsperada, int cantidad)
    {
        RegistroError err = new RegistroError();
        err.setArchivo(nodo.getArchivo());
        err.setFila(nodo.getFila());
        err.setColumna(nodo.getColumna());
        err.setTipo("Semantico");
        err.setDescripcion("Se esperaban " + cantidadEsperada + " parametros y se encontraron " + cantidad + ".");
        Entorno.addError(err);
    }
    
    public static void errorTipoParametro(Nodo nodo, String tipo1, String tipo2)
    {
        RegistroError err = new RegistroError();
        err.setArchivo(nodo.getArchivo());
        err.setFila(nodo.getFila());
        err.setColumna(nodo.getColumna());
        err.setTipo("Semantico");
        err.setDescripcion("Se esperaban tipo " + tipo1 + " y se encontro parametro tiopo " + tipo2 + ".");
        Entorno.addError(err);
    }
    
}
