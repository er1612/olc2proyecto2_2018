/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Asignacion {
    
    public static void ejecutarAsignacion(Nodo nodo)
    {
        String id = nodo.getToken();
        Variable variable = null;

        for(Variable variableExistente : EjecutarDraco.ambito)
        {
            if(variableExistente.getId().equals(id))
            {
                variable = variableExistente;
                break;
            }
        }
        
        if(variable==null)
        {
            RegistroError error = new RegistroError();
            error.setArchivo(nodo.getArchivo());
            error.setFila(nodo.getFila());
            error.setColumna(nodo.getColumna());
            error.setTipo("Semantico");
            error.setDescripcion("No se encontro la variable " + nodo.getToken() + ".");
            Entorno.addError(error);
            return;
        }
        
        Dato valor = Expresiones.nodoExpresion(nodo.getNodo(0));
        
        if(valor.getTipo().equalsIgnoreCase("nulo"))
        {
            RegistroError error = new RegistroError();
            error.setArchivo(nodo.getArchivo());
            error.setFila(nodo.getFila());
            error.setColumna(nodo.getColumna());
            error.setTipo("Semantico");
            error.setDescripcion("El valor a asignar es nulo.");
            Entorno.addError(error);
            return;
        }
        
        variable.setValor(valor);
    }
}
