/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Declaracion {
    
    public static void ejecutarDeclaracion(Nodo nodo)
    {
        for(Nodo nodoVariable : nodo.getNodos())
        {
            String id = nodoVariable.getToken();
            boolean existe = false;
            
            for(Variable var : EjecutarDraco.ambito)
            {
                if(var.getId().equals(id))
                {
                    existe = true;
                }
            }
            
            if(existe)
            {
                RegistroError error = new RegistroError();
                error.setArchivo(nodoVariable.getArchivo());
                error.setFila(nodoVariable.getFila());
                error.setColumna(nodoVariable.getColumna());
                error.setTipo("Semantico");
                error.setDescripcion("Ya se declaro la variable " + id + ".");
                Entorno.addError(error);
                continue;
            }
            
            Variable variable = new Variable();
            variable.setId(id);
            
            if(nodoVariable.getNombre().equalsIgnoreCase("variable_expresion"))
            {
                Dato valor = Expresiones.nodoExpresion(nodoVariable.getNodo(0));
                variable.setValor(valor);
            }
            
            EjecutarDraco.ambito.add(variable);
        }
    }
    
}
