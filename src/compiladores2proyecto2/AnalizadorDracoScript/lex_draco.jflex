package compiladores2proyecto2.AnalizadorDracoScript;
import java_cup.runtime.Symbol;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.Entorno;
import java.awt.Color;
import compiladores2proyecto2.RegistroError;
%%
%cupsym sim_draco
%class lex_draco
%cup
%public
%unicode
%line
%char
%column
%ignorecase

%{
    public static String archivo = "";

	public void PrintToken(String str){
		System.out.println(str);
	}
%}

digito				=	[0-9]
entero 				=	([0-9])+
ndecimal			=	([0-9])+("."([0-9])+)
cadena				=	"\"" ([^\"])* "\""
caracter			=	"'" (.) "'"
comentario 			=	"$$" (.)* "\n"
comentario_multi	=	"$*" ([^])* "*$"
identificador		=	([a-zA-Z]|"_")([a-zA-Z]|[0-9]|"_")*

%%
{entero}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#721783"));
	return new Symbol(sim_draco.entero, yycolumn,yyline,new String(yytext()));
}

{ndecimal}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#721783"));
	return new Symbol(sim_draco.ndecimal, yycolumn,yyline,new String(yytext()));
}

{cadena}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#e17d00"));
	return new Symbol(sim_draco.cadena, yycolumn,yyline,new String(yytext()));
}

{caracter}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#e17d00"));
	return new Symbol(sim_draco.caracter, yycolumn,yyline,new String(yytext()));
}

//Expresiones Booleanas----------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"true"
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#151e96"));
	return new Symbol(sim_draco.rtrue, yycolumn,yyline,new String(yytext()));
}

"false"
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#151e96"));
	return new Symbol(sim_draco.rfalse, yycolumn,yyline,new String(yytext()));
}

//Operadores Aritmeticos---------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"+"					{return new Symbol(sim_draco.mas, yycolumn,yyline,new String(yytext()));}
"-"					{return new Symbol(sim_draco.menos, yycolumn,yyline,new String(yytext()));}
"*"					{return new Symbol(sim_draco.por, yycolumn,yyline,new String(yytext()));}
"/"					{return new Symbol(sim_draco.dividido, yycolumn,yyline,new String(yytext()));}
"%"					{return new Symbol(sim_draco.modulo, yycolumn,yyline,new String(yytext()));}
"^"					{return new Symbol(sim_draco.potencia, yycolumn,yyline,new String(yytext()));}
"++"				{return new Symbol(sim_draco.adicion, yycolumn,yyline,new String(yytext()));}
"--"				{return new Symbol(sim_draco.sustraccion, yycolumn,yyline,new String(yytext()));}
//Signos de Agrupacion-----------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"("					{return new Symbol(sim_draco.a_parentesis, yycolumn,yyline,new String(yytext()));}
")"					{return new Symbol(sim_draco.c_parentesis, yycolumn,yyline,new String(yytext()));}
//Operadores Realcionales--------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
">"					{return new Symbol(sim_draco.mayor, yycolumn,yyline,new String(yytext()));}
"<"					{return new Symbol(sim_draco.menor, yycolumn,yyline,new String(yytext()));}
">="				{return new Symbol(sim_draco.mayor_igual, yycolumn,yyline,new String(yytext()));}
"<="				{return new Symbol(sim_draco.menor_igual, yycolumn,yyline,new String(yytext()));}
"=="				{return new Symbol(sim_draco.igual_relacional, yycolumn,yyline,new String(yytext()));}
"!="				{return new Symbol(sim_draco.diferente, yycolumn,yyline,new String(yytext()));}
//Operadores Logicos-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"&&"				{return new Symbol(sim_draco.and, yycolumn,yyline,new String(yytext()));}
"||"				{return new Symbol(sim_draco.or, yycolumn,yyline,new String(yytext()));}
"!"					{return new Symbol(sim_draco.not, yycolumn,yyline,new String(yytext()));}
//Declaracion--------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"var"				{return new Symbol(sim_draco.var, yycolumn,yyline,new String(yytext()));}
":=:"				{return new Symbol(sim_draco.igual, yycolumn,yyline,new String(yytext()));}
//Sentencia If-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"if"				{return new Symbol(sim_draco.rif, yycolumn,yyline,new String(yytext()));}
"elif"				{return new Symbol(sim_draco.elif, yycolumn,yyline,new String(yytext()));}
"not"				{return new Symbol(sim_draco.ifnot, yycolumn,yyline,new String(yytext()));}
//Ciclos-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"smash"				{return new Symbol(sim_draco.smash, yycolumn,yyline,new String(yytext()));}
"while"				{return new Symbol(sim_draco.rwhile, yycolumn,yyline,new String(yytext()));}
"for"				{return new Symbol(sim_draco.rfor, yycolumn,yyline,new String(yytext()));}
//Palabras reservadas------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"Print"				{return new Symbol(sim_draco.rprint, yycolumn,yyline,new String(yytext()));}
"RunMultDasm"		{return new Symbol(sim_draco.runmultdasm, yycolumn,yyline,new String(yytext()));}
"Point"				{return new Symbol(sim_draco.point, yycolumn,yyline,new String(yytext()));}
"Quadrate"			{return new Symbol(sim_draco.quadrate, yycolumn,yyline,new String(yytext()));}
"Oval"				{return new Symbol(sim_draco.oval, yycolumn,yyline,new String(yytext()));}
"String"			{return new Symbol(sim_draco.rstring, yycolumn,yyline,new String(yytext()));}
"Line"				{return new Symbol(sim_draco.rline, yycolumn,yyline,new String(yytext()));}
//No son del lenguaje------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
":"					{return new Symbol(sim_draco.dospuntos, yycolumn,yyline,new String(yytext()));}
";"					{return new Symbol(sim_draco.puntocoma, yycolumn,yyline,new String(yytext()));}
"{"					{return new Symbol(sim_draco.a_llave, yycolumn,yyline,new String(yytext()));}
"}"					{return new Symbol(sim_draco.c_llave, yycolumn,yyline,new String(yytext()));}
","					{return new Symbol(sim_draco.coma, yycolumn,yyline,new String(yytext()));}

{identificador}		{return new Symbol(sim_draco.identificador, yycolumn,yyline,new String(yytext()));}

{comentario}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#737373"));
}

{comentario_multi}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#737373"));
}

[ \t\r\f\n]+		{}

.
{
	RegistroError err = new RegistroError();
    err.setArchivo(Entorno.getArchivo());
    err.setFila(yyline);
    err.setColumna(yycolumn);
    err.setTipo("Lexico");
    err.setDescripcion("No se reconoce el simbolo " + yytext() + " .");
    Entorno.addError(err);
}
