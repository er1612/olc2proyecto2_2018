/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class CicloWhile {
    
    public static void ejecutarWhile(Nodo nodo)
    {
        //Ejecutando el primer IF
        Nodo nodoExpresion = nodo.getNodo(0);
        Nodo nodoSentencias = nodo.getNodo(1);
        
        //Para primera iteracion
        Dato expresion = Expresiones.nodoExpresion(nodoExpresion);
        
        //Comprobando que el dato sea booleano
        if(!expresion.getTipo().equalsIgnoreCase("booleano"))
        {
            EjecutarDraco.reportarErrorBooleano(nodoExpresion);
            return;
        }
        
        while(expresion.getValor().equalsIgnoreCase("1"))
        {
            //Ambito
            int cantidad = EjecutarDraco.ambito.size();
            
            int resultado = EjecutarDraco.ejecutarSentencias(nodoSentencias);
            EjecutarDraco.restaurarAmbito(cantidad);
            
            if(resultado == 2 )
            {
                break;
            }
            
            //Para n iteracion
            expresion = Expresiones.nodoExpresion(nodoExpresion);
        
            //Comprobando que el dato sea booleano
            if(!expresion.getTipo().equalsIgnoreCase("booleano"))
            {
                EjecutarDraco.reportarErrorBooleano(nodoExpresion);
                return;
            }
        }
    }
}
