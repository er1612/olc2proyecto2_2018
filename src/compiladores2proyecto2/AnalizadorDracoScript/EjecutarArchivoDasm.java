/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.AnalizadorDasm.lex_dasm;
import compiladores2proyecto2.AnalizadorDasm.sin_dasm;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.VentanaPrincipal;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rodolfo
 */
public class EjecutarArchivoDasm {
    
    public static void ejecutarSentencia(Nodo nodo) throws FileNotFoundException, IOException
    {
        Nodo nodoParametros = nodo.getNodo(0);
        
        for(Nodo parametro : nodoParametros.getNodos())
        {
            Dato nombre = Expresiones.nodoExpresion(parametro);
            
            String nom = nombre.getValor();
            String rut = Entorno.tabEjecucion.getRutaArchivo();
            String rutnom = rut + "/" + nom;
            
            System.out.println(rutnom);
            
            File archivo = new File(rutnom);
            
            if(archivo.exists() && !archivo.isDirectory()) { 
                System.out.println(rutnom);
                BufferedReader br = null;
                    
                br = new BufferedReader(new FileReader(rutnom));

                StringBuilder sb = new StringBuilder();

                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = br.readLine();
                }   

                String texto = sb.toString();
                String nombreArchivo = archivo.getName();
                String rutaArchivo = archivo.getParent();
                String tipoArchivo = getExtension(archivo);
                Entorno.ventanaPrincipal.crearTab(nombreArchivo, rutaArchivo, tipoArchivo, texto);
                Entorno.setArchivo(nombreArchivo);
                analizarDasm(texto);
            }
        }
    }
    
    private static String getExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
    
    private static void analizarDasm(String texto){
        try {
            new sin_dasm(new lex_dasm(new StringReader(texto))).parse();
        } catch (Exception ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
