/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Expresiones {
    
    public static Dato nodoExpresion(Nodo nodo)
    {
        String nombreExpresion = nodo.getNombre().toLowerCase();
        
        switch(nombreExpresion)
        {
            case "dato":
                return getDato(nodo);
            case "expresion_binaria":
                return getResultadoBinario(nodo);
            case "expresion_unaria_pre":
                return getResultadoUnarioPre(nodo);
            case "expresion_unaria_post":
            case "aumento":
            case "reduccion":
                return getResultadoUnarioPost(nodo);
        }

        return getDatoNulo();
    }
    
    public static Dato getResultadoBinario(Nodo nodo)
    {
        String operador = nodo.getToken();
        
        Dato val1 = nodoExpresion(nodo.getNodo(0));
        Dato val2 = nodoExpresion(nodo.getNodo(1));
        
        switch(operador)
        {
            case "+":
                return getSuma(nodo, val1, val2);
            case "-":
                return getResta(nodo, val1, val2);
            case "*":
                return getMultiplicacion(nodo, val1, val2);
            case "/":
                return getDivision(nodo, val1, val2);
            case "^":
                return getPotencia(nodo, val1, val2);
            case "%":
                return getModulo(nodo, val1, val2);
            case "<":
                return getMenor(nodo, val1, val2);
            case ">":
                return getMayor(nodo, val1, val2);
            case "<=":
                return getMenorIgual(nodo, val1, val2);
            case ">=":
                return getMayorIgual(nodo, val1, val2);
            case "==":
                return getIgual(nodo, val1, val2);
            case "!=":
                return getDiferente(nodo, val1, val2);
            case "&&":
                return getAnd(nodo, val1, val2);
            case "||":
                return getOr(nodo, val1, val2);
        }
        
        return getDatoNulo();
    }
    
    public static Dato getResultadoUnarioPre(Nodo nodo)
    {
        String operador = nodo.getToken();
        
        Dato val = nodoExpresion(nodo.getNodo(0));
        
        switch(operador)
        {
            case "!":
                return getNot(nodo, val);
            case "+":
                return getPositivo(nodo, val);
            case "-":
                return getNegativo(nodo, val);
        }
        return getDatoNulo();
    }
    
    public static Dato getResultadoUnarioPost(Nodo nodo)
    {
        String operador = nodo.getToken();
        
        Dato val = nodoExpresion(nodo.getNodo(0));
        
        switch(operador)
        {
            case "++":
                return getAumento(nodo, val);
            case "--":
                return getReduccion(nodo, val);
        }
        return getDatoNulo();
    }
    
    //Operaciones ARITMETICAS----------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    public static Dato getSuma(Nodo operador, Dato val1, Dato val2)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 + v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 + v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 + v2));
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = Integer.parseInt(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 + v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 + v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = (int)val1.getValor().charAt(0);
            int v2 = Integer.parseInt(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 + v2));
            
            return dato;
        }
        //Resultados de tipo CADENA
        else if(val1.getTipo().equalsIgnoreCase("cadena") || val2.getTipo().equalsIgnoreCase("cadena")&&
                !val1.getTipo().equalsIgnoreCase("booleano")&&
                !val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            dato.setValor(String.valueOf(val1.getValor() + val2.getValor()));
            
            return dato;
        }
        //Resultados de tipo BOOLEANO
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = Integer.parseInt(val2.getValor());
            int resultado = 0;
            
            if(v1==1 || v2==1)
            {
                resultado = 1;
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("SUMA", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getResta(Nodo operador, Dato val1, Dato val2)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 - v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 - v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 - v2));
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = Integer.parseInt(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 - v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 - v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = (int)val1.getValor().charAt(0);
            int v2 = Integer.parseInt(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 - v2));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("RESTA", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMultiplicacion(Nodo operador, Dato val1, Dato val2)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 * v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 * v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 * v2));
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = Integer.parseInt(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 * v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 * v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = (int)val1.getValor().charAt(0);
            int v2 = Integer.parseInt(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 * v2));
            
            return dato;
        }
        //Resultados de tipo BOOLEANO
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = Integer.parseInt(val2.getValor());
            int resultado = 0;
            
            if(v1==1 && v2==1)
            {
                resultado = 1;
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("MULTIPLICACION", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getDivision(Nodo operador, Dato val1, Dato val2)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 / v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 / v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 / v2));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("DIVISION", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getModulo(Nodo operador, Dato val1, Dato val2)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 % v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 % v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(v1 % v2));
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = Integer.parseInt(val2.getValor());
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 % v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = Integer.parseInt(val1.getValor());
            int v2 = (int)val2.getValor().charAt(0);
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 % v2));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = (int)val1.getValor().charAt(0);
            int v2 = Integer.parseInt(val2.getValor());
            
            if(v2==0)
            {
                reportarErrorCero(operador);
                return getDatoNulo();
            }
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(v1 % v2));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("MODULO", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getPotencia(Nodo operador, Dato val1, Dato val2)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(Math.pow(v1, v2)));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(Math.pow(v1, v2)));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf( Math.pow(v1, v2)));
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano")||
                val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Integer.parseInt(val1.getValor());
            double v2 = Integer.parseInt(val2.getValor());
            Double resultado = Math.pow(v1, v2);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Integer.parseInt(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            Double resultado = Math.pow(v1, v2);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Integer.parseInt(val2.getValor());
            Double resultado = Math.pow(v1, v2);
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("POTENCIA", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getPositivo(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val.getValor());
            int resultado = +1 * v1;
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val.getValor());
            double resultado = +1 * v1;
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("POSITIVO ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getNegativo(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val.getValor());
            int resultado = -1 * v1;
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val.getValor());
            double resultado = -1 * v1;
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("NEGATIVO ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getAumento(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val.getValor());
            int resultado = v1 + 1;
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val.getValor());
            double resultado = v1 + 1;
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("AUMENTO (++) ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getReduccion(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            int v1 = Integer.parseInt(val.getValor());
            int resultado = v1 - 1;
            
            Dato dato = new Dato();
            dato.setTipo("entero");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            double v1 = Double.parseDouble(val.getValor());
            double resultado = v1 - 1;
            
            Dato dato = new Dato();
            dato.setTipo("decimal");
            dato.setValor(String.valueOf(resultado));
            
            return dato;
        }
        else
        {
            reportarErrorTipos("REDUCCION (--) ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getDato(Nodo nodo)
    {
        if(nodo.getTipo().equalsIgnoreCase("id"))
        {
            for(Variable variable : EjecutarDraco.ambito)
            {
                if(variable.getId().equals(nodo.getToken()))
                {
                    Dato newDato = new Dato();
                    newDato.setTipo(variable.getValor().getTipo());
                    newDato.setValor(variable.getValor().getValor());
                    
                    return newDato;
                }
            }
            
            RegistroError error = new RegistroError();
            error.setArchivo(nodo.getArchivo());
            error.setFila(nodo.getFila());
            error.setColumna(nodo.getColumna());
            error.setTipo("Semantico");
            error.setDescripcion("No se encontro la variable " + nodo.getToken() + ".");
            Entorno.addError(error);
            
            return getDatoNulo();
        }
        else
        {
            Dato dato = new Dato();
            dato.setTipo(nodo.getTipo());
            dato.setValor(nodo.getToken());
            return dato;
        }
    }
    
    public static Dato getDatoNulo()
    {
        Dato dato = new Dato();
        dato.setTipo("nulo");
        dato.setValor("");
        return dato;
    }
    
    //Operaciones RELACIONALES---------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    public static Dato getMayor(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 > v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 > v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 > v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = 0;
            int v2 = 0;
            
            for(int x=0; x<val1.getValor().length(); x++)
            {
                v1 += (int)val1.getValor().charAt(x);
            }
            
            for(int x=0; x<val2.getValor().length(); x++)
            {
                v2 += (int)val2.getValor().charAt(x);
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 > v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MAYOR (>)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMenor(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 < v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 < v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 < v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = 0;
            int v2 = 0;
            
            for(int x=0; x<val1.getValor().length(); x++)
            {
                v1 += (int)val1.getValor().charAt(x);
            }
            
            for(int x=0; x<val2.getValor().length(); x++)
            {
                v2 += (int)val2.getValor().charAt(x);
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 < v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MENOR (<)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMayorIgual(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 >= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 >= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 >= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = 0;
            int v2 = 0;
            
            for(int x=0; x<val1.getValor().length(); x++)
            {
                v1 += (int)val1.getValor().charAt(x);
            }
            
            for(int x=0; x<val2.getValor().length(); x++)
            {
                v2 += (int)val2.getValor().charAt(x);
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 >= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MAYOR O IGUAL (>=)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMenorIgual(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 <= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 <= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 <= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = 0;
            int v2 = 0;
            
            for(int x=0; x<val1.getValor().length(); x++)
            {
                v1 += (int)val1.getValor().charAt(x);
            }
            
            for(int x=0; x<val2.getValor().length(); x++)
            {
                v2 += (int)val2.getValor().charAt(x);
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 <= v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MENOR O IGUAL (<=)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getIgual(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 == v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 == v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 == v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = 0;
            int v2 = 0;
            
            for(int x=0; x<val1.getValor().length(); x++)
            {
                v1 += (int)val1.getValor().charAt(x);
            }
            
            for(int x=0; x<val2.getValor().length(); x++)
            {
                v2 += (int)val2.getValor().charAt(x);
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 == v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION IGUAL (==)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getDiferente(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 != v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            double v1 = Double.parseDouble(val1.getValor());
            double v2 = (int)val2.getValor().charAt(0);
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 != v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            double v1 = (int)val1.getValor().charAt(0);
            double v2 = Double.parseDouble(val2.getValor());
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 != v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            int v1 = 0;
            int v2 = 0;
            
            for(int x=0; x<val1.getValor().length(); x++)
            {
                v1 += (int)val1.getValor().charAt(x);
            }
            
            for(int x=0; x<val2.getValor().length(); x++)
            {
                v2 += (int)val2.getValor().charAt(x);
            }
            
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(v1 != v2)
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION DIFERENTE (!=)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    //Operaciones RELACIONALES---------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    
    public static Dato getAnd(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(val1.getValor().equalsIgnoreCase("1") && val2.getValor().equalsIgnoreCase("1"))
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("LOGICA AND (&&)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getOr(Nodo operador, Dato val1, Dato val2)
    {
        if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(val1.getValor().equalsIgnoreCase("1") || val2.getValor().equalsIgnoreCase("1"))
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("LOGICA AND (&&)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getNot(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            dato.setValor("0");
            
            if(val.getValor().equalsIgnoreCase("0"))
            {
                dato.setValor("1");
            }
            
            return dato;
        }
        else
        {
            reportarErrorTipos("LOGICA NOT (!)", operador, val, null);
        }
        return getDatoNulo();
    }
    
    //Errores de tipo y operacion------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    public static void reportarErrorTipos(String operacion, Nodo operador, Dato val1, Dato val2)
    {
        String tipoVal2 = "";
        
        if(val2!=null)
        {
            tipoVal2 =  " y " + val2.getTipo();
        }
        
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se puede hacer la operacion " + operacion + " con valores de tipo " + 
                val1.getTipo() + tipoVal2 + ".");
        Entorno.addError(error);
    }
    
    public static void reportarErrorCero(Nodo operador)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se puede DIVIDIR un valor entre 0.");
        Entorno.addError(error);
    }
    
}
