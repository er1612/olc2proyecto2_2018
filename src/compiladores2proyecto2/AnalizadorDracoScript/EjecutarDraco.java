/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Dibujo;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Imagen;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;
import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rodolfo
 */
public class EjecutarDraco{
    
    public static LinkedList<Variable> ambito = new LinkedList<Variable>();
    public static LinkedList<Dibujo> dibujos = new LinkedList<Dibujo>();
    public static Imagen imagenDraco = null;
    
    public static void Ejecutar(Nodo nodo)
    {
        imagenDraco = new Imagen();
        imagenDraco.setVisible(true);

        ambito.clear();
        dibujos.clear();

        ejecutarSentencias(nodo);
    }
    
    public static int ejecutarSentencias(Nodo nodo)
    {
        for(Nodo sentencia : nodo.getNodos())
        {
            String nombre = sentencia.getNombre().toLowerCase();
            switch(nombre)
            {
                case "print":
                    FuncionesPropias.funcionPrint(sentencia);
                    break;
                case "declaracion":
                    Declaracion.ejecutarDeclaracion(sentencia);
                    break;
                case "asignacion":
                    Asignacion.ejecutarAsignacion(sentencia);
                    break;
                case "sentencia_if":
                    int resultadoIf = SentenciaIf.ejecutarIf(sentencia);
                    if(resultadoIf == 2)
                    {
                        return 2;
                    }
                    break;
                case "sentencia_smash":
                    return 2;
                case "sentencia_while":
                    CicloWhile.ejecutarWhile(sentencia);
                    break;
                case "sentencia_for":
                    CicloFor.ejecutarFor(sentencia);
                    break;
                case "aumento":
                case "reduccion":
                    AumentoReduccion.ejecutarAumentoReduccion(sentencia);
                    break;
                case "punto":
                    FuncionesPropias.dibujarPunto(sentencia);
                    break;
                case "rectangulo":
                    FuncionesPropias.dibujarRectangulo(sentencia);
                    break;
                case "circulo":
                    FuncionesPropias.dibujarCirculo(sentencia);
                    break;
                case "cadena":
                    FuncionesPropias.dibujarCadena(sentencia);
                    break;
                case "linea":
                    FuncionesPropias.dibujarLinea(sentencia);
                    break;
                case "run_dasm":
                    {
                        try {
                            EjecutarArchivoDasm.ejecutarSentencia(sentencia);
                        } catch (IOException ex) {
                            Logger.getLogger(EjecutarDraco.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
            }
            
            if(Entorno.esDepuracion)
            {
                Entorno.pintarLinea(sentencia.getArchivo(), sentencia.getFila());
            }
            
        }
        
        return 1;
    }
    
    public static void restaurarAmbito(int cantidad)
    {
        while(ambito.size() > cantidad)
        {
            ambito.removeLast();
        }
    }
    
    public static void reportarErrorBooleano(Nodo nodoExpresion)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(nodoExpresion.getArchivo());
        error.setFila(nodoExpresion.getFila());
        error.setColumna(nodoExpresion.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("La expresion debe ser de tipo BOOLEANA");
        Entorno.addError(error);
    }
}
