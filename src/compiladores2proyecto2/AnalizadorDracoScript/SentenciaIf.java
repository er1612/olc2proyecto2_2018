/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class SentenciaIf {
    
    public static int ejecutarIf(Nodo nodo)
    {
        //Ejecutando el primer IF
        Nodo nodoExpresion = nodo.getNodo(0);
        Nodo nodoSentencias = nodo.getNodo(1);
        Dato expresion = Expresiones.nodoExpresion(nodoExpresion);
        
        int resultado = comprobarEjecutar(expresion, nodoExpresion, nodoSentencias);
        
        if(resultado>0)
        {
            return resultado;
        }
        
        Nodo nodoElif = null;
        Nodo nodoIfNot = null;
        
        if(nodo.getNodos().size()>2)
        {
            for(int x=2; x<nodo.getNodos().size(); x++)
            {
                if(nodo.getNodo(x).getNombre().equalsIgnoreCase("sentencias_elif"))
                {
                    nodoElif = nodo.getNodo(x);
                }
                if(nodo.getNodo(x).getNombre().equalsIgnoreCase("sentencia_else"))
                {
                    nodoIfNot = nodo.getNodo(x);
                }
            }
        }
        
        //Listas de sentencias ELIF
        
        if(nodoElif!=null)
        {
            for(Nodo elif : nodoElif.getNodos())
            {
                resultado = ejecutarElif(elif);
                
                if(resultado>0)
                {
                    return resultado;
                }
            }
        }
        
        //Sentencia IF NOT
        
        if(nodoIfNot!=null)
        {
            //Como si se debe ejecutar se envia un dato booleano verdadero
            Dato expresionIfNot = new Dato();
            expresionIfNot.setTipo("booleano");
            expresionIfNot.setValor("1");
            
            //Como no hay expresion se envia un nodo expresion vacio
            Nodo nodoVacio = new Nodo();
            
            comprobarEjecutar(expresionIfNot, nodoVacio, nodoIfNot);
        }
        
        return 1;
    }
    
    private static int ejecutarElif(Nodo nodo)
    {
        Nodo nodoExpresion = nodo.getNodo(0);
        Nodo nodoSentencias = nodo.getNodo(1);
        Dato expresion = Expresiones.nodoExpresion(nodoExpresion);
        
        return comprobarEjecutar(expresion, nodoExpresion, nodoSentencias);
    }
    
    private static int comprobarEjecutar(Dato expresion, Nodo nodoExpresion, Nodo nodoSentencias)
    {
        int cantidad = EjecutarDraco.ambito.size();
        
        if(!expresion.getTipo().equalsIgnoreCase("booleano"))
        {
            EjecutarDraco.reportarErrorBooleano(nodoExpresion);
            return 1;
        }
        
        if(expresion.getValor().equalsIgnoreCase("1"))
        {
            int resultado = EjecutarDraco.ejecutarSentencias(nodoSentencias);
            EjecutarDraco.restaurarAmbito(cantidad);
            return resultado;
        }
        
        return 0;
    }
    
}
