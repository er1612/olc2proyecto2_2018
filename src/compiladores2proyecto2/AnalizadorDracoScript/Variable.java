/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

/**
 *
 * @author rodolfo
 */
public class Variable {
    
    private String id = "";
    private Dato valor = Expresiones.getDatoNulo();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dato getValor() {
        return valor;
    }

    public void setValor(Dato valor) {
        this.valor = valor;
    }
    
}
