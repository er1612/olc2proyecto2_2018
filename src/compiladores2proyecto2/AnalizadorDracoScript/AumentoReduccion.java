/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDracoScript;

import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class AumentoReduccion {
    
    public static void ejecutarAumentoReduccion(Nodo nodo)
    {
        Dato dato = Expresiones.nodoExpresion(nodo);
        
        String id = nodo.getNodo(0).getToken();
        
        for(Variable variable : EjecutarDraco.ambito)
        {
            if(id.equals(variable.getId()))
            {
                variable.setValor(dato);
                return;
            }
        }
        
        RegistroError error = new RegistroError();
        error.setArchivo(nodo.getArchivo());
        error.setFila(nodo.getFila());
        error.setColumna(nodo.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se encontro la variable " + id + ".");
        Entorno.addError(error);
        return;
        
    }
}
