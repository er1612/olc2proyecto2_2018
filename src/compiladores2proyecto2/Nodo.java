/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2;

import compiladores2proyecto2.AnalizadorDpp.Ambito;
import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class Nodo {
    private String nombre = "";
    private String token = "";
    private String tipo = "";
    private int fila = 0;
    private int columna = 0;
    private String archivo = "";
    private LinkedList<Nodo> nodos = new LinkedList<Nodo>();
    private Ambito ambitoLocal = new Ambito();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getArchivo() {
        return archivo;
    }

    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }
    
    public Nodo getNodo(int indice)
    {
        if(this.nodos.size()>0 && indice>=0 && indice<this.nodos.size())
        {
            return this.nodos.get(indice);
        }
        return null;
    }
    
    public void addNodo(Nodo nodo)
    {
        this.nodos.addLast(nodo);
    }

    public LinkedList<Nodo> getNodos() {
        return nodos;
    }

    public void setNodos(LinkedList<Nodo> nodos) {
        this.nodos = nodos;
    }

    public Ambito getAmbitoLocal() {
        return ambitoLocal;
    }

    public void setAmbitoLocal(Ambito ambitoLocal) {
        this.ambitoLocal = ambitoLocal;
    }
    
}
