/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2;

import compiladores2proyecto2.AnalizadorDasm.EjecutarDasm;
import java.awt.Color;
import java.awt.Font;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 *
 * @author rodolfo
 */
public class Entorno extends Thread{
    
    private static String archivo = "archivo.txt";
    private static Nodo nodo = null;
    public static VentanaPrincipal ventanaPrincipal = null;
    private static int VelocidadEjecucion = 100;
    public static Tab tabEjecucion = null;
    public static Thread hilo ;
    public static boolean esDepuracion = false;
    
    private static LinkedList<RegistroError> errores = new LinkedList<RegistroError>();

    public static Nodo getNodo() {
        return nodo;
    }

    public static void setNodo(Nodo nodo) {
        Entorno.nodo = nodo;
    }

    public static String getArchivo() {
        return archivo;
    }

    public static void setArchivo(String archivo) {
        Entorno.archivo = archivo;
    }
    
    public static void addError(RegistroError error)
    {
        errores.addLast(error);
        ventanaPrincipal.reportarErrores();
    }

    public static LinkedList<RegistroError> getErrores() {
        return errores;
    }

    public static VentanaPrincipal getVentanaPrincipal() {
        return ventanaPrincipal;
    }

    public static void setVentanaPrincipal(VentanaPrincipal ventanaPrincipal) {
        Entorno.ventanaPrincipal = ventanaPrincipal;
    }
    
    public static void cambiarColorTexto(int inicio, int longitud, Color color)
    {
        StyledDocument sdoc = ventanaPrincipal.listaTabs.get(ventanaPrincipal.indiceTab()).getTextArea().getStyledDocument();
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, color);
        sdoc.setCharacterAttributes(inicio, longitud, attrs, false);
    }
    
    public static void restaurarColorTexto()
    {
        StyledDocument sdoc = ventanaPrincipal.listaTabs.get(ventanaPrincipal.indiceTab()).getTextArea().getStyledDocument();
        JTextPane jtp = ventanaPrincipal.listaTabs.get(ventanaPrincipal.indiceTab()).getTextArea();
        SimpleAttributeSet attrs = new SimpleAttributeSet();
        StyleConstants.setForeground(attrs, Color.black);
        sdoc.setCharacterAttributes(0, jtp.getText().length(), attrs, false);
    }

    public static int getVelocidadEjecucion() {
        return VelocidadEjecucion;
    }

    public static void setVelocidadEjecucion(int VelocidadEjecucion) {
        int velocidad = VelocidadEjecucion;
        if(velocidad<100)
        {
            velocidad = 10;
        }
        
        Entorno.VelocidadEjecucion = velocidad;
    }
    
    public static void pintarLinea(String nombre, int linea)
    {
        if(!esDepuracion)
        {
            return;
        }
        
        Tab tabActual = null;
        
        for(Tab tab : ventanaPrincipal.listaTabs)
        {
            if(tab.getNombreArchivo().equals(nombre))
            {
                tabActual = tab;
                break;
            }
        }
        
        if(tabActual==null)
        {
            return;
        }
        
        int indice = ventanaPrincipal.listaTabs.indexOf(tabActual);
        ventanaPrincipal.tabsEntradas.setSelectedIndex(indice);
        pintarLinea(linea, tabActual);
        int velocidad = (Integer) ventanaPrincipal.velocidad.getValue();
        
        try {
            Thread.sleep(velocidad);
        } catch (InterruptedException ex) {
            Logger.getLogger(Entorno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void pausarEjecucion()
    {
        try {
            hilo.wait();
        } catch (InterruptedException ex) {
            Logger.getLogger(Entorno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void continuarEjecucion()
    {
        hilo.notify();
    }
    
    private static void pintarLinea(int linea, Tab tab)
    {
        try {
            int cantidadLineas = 0;
            int inicioLinea = 0;
            int finLinea = 0;
            String texto;

            JTextPane panel = tab.getTextArea();
            panel.getHighlighter().removeAllHighlights();
            
            texto = panel.getText();

            for(int x=0; x<texto.length(); x++)
            {
                if(texto.charAt(x)=='\n')
                {
                    cantidadLineas++;

                    if(cantidadLineas == linea)
                    {
                        inicioLinea = x+1;
                    }

                    if(cantidadLineas == linea + 1)
                    {
                        finLinea = x;
                        break;
                    }
                }
            }
            
            boolean esPunto = false;
            
            for(int punto : tab.puntosInterrupcion)
            {
                if(punto==linea)
                {
                    esPunto = true;
                    break;
                }
            }
            
            if(esPunto)
            {
                panel.getHighlighter().addHighlight(inicioLinea, finLinea,
                    new DefaultHighlighter.DefaultHighlightPainter(java.awt.Color.RED));
                
                pausarEjecucion();
            }
            else
            {
                panel.getHighlighter().addHighlight(inicioLinea, finLinea,
                    new DefaultHighlighter.DefaultHighlightPainter(java.awt.Color.YELLOW));
            }
            
            
        } catch (BadLocationException ex) {
            
        }
    }
    
}
