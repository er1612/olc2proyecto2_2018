/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.Asignacion.getVariable;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class SentenciaAumento {
    
    public static void generarSentencia(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String id =  nodo.getToken();
        Simbolo simbolo = getVariable(id, ambito);

        escribir("//Aumentar variable " + id);
        
        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
        {
            escribir("Get_local " + simbolo.getPosicion());
        }
        else
        {
            escribir("Get_local 0");
            escribir(String.valueOf(simbolo.getPosicion()));
            escribir("Add");
            escribir("Get_local $calc");
        }
        
        escribir("1");
        escribir("Add");
        
        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
        {
            escribir("Set_local " + simbolo.getPosicion());
        }
        else
        {
            escribir("Get_local 0");
            escribir(String.valueOf(simbolo.getPosicion()));
            escribir("Add");
            escribir("Set_local $calc");
        }
    }
}
