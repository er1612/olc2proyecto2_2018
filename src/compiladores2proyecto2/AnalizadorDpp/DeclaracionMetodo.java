/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.Declaracion.variableExiste;
import compiladores2proyecto2.Nodo;
import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class DeclaracionMetodo {
    
    public static void ejecutarDeclaracionMetodo(Nodo nodo)
    {
        String id = nodo.getToken();
        
        for(Nodo parametro : nodo.getNodo(0).getNodos())
        {
            id += "_" + parametro.getTipo();
        }
        
        Metodo metodo = new Metodo();
        metodo.setNombre(id);
        metodo.setParametros(nodo.getNodo(0));
        metodo.setTipo(nodo.getTipo());
        metodo.setSentencias(nodo.getNodo(1));
        
        metodo.getAmbitoGeneral().setNombreAmbito(id);
        metodo.getAmbitoPrincipal().setNombreAmbito(id);
        
        declararRetorno(metodo);
        
        declaracionParametros(metodo.getParametros(), metodo);
        EjecutarDpp.metodos.addLast(metodo);
        
        buscarDeclaraciones(nodo.getNodo(1), metodo, metodo.getAmbitoPrincipal());
    }
    
    public static void declararRetorno(Metodo metodo)
    {
        Simbolo sim = new Simbolo();
        sim.setNombreAmbito(metodo.getNombre());
        sim.setNombre("retorno");
        sim.setPosicion(metodo.getTamano());
        sim.setRol("retorno");
        sim.setTamano(1);
        sim.setTipo(metodo.getTipo());
        
        metodo.getAmbitoPrincipal().addSimbolo(sim);
        metodo.getAmbitoGeneral().addSimbolo(sim);
    }
    
    public static void declaracionParametros(Nodo nodo, Metodo metodo)
    {
        for(Nodo variable : nodo.getNodos())
        {
            metodo.setCantParametros(metodo.getCantParametros() + 1);
            
            if(variableExiste(variable.getToken(), metodo.getAmbitoPrincipal()))
            {
                System.out.println("La variable: " + variable.getToken() + " ya existe.");
                return;
            }
            
            String tipo = variable.getTipo();
            
            Simbolo sim = new Simbolo();
            sim.setNombreAmbito(metodo.getNombre());
            sim.setNombre(variable.getToken());
            metodo.aumentarTamano(1);
            sim.setPosicion(metodo.getTamano());
            sim.setRol("variable");
            sim.setTamano(1);
            sim.setTipo(tipo);
            
            metodo.getAmbitoPrincipal().addSimbolo(sim);
            metodo.getAmbitoGeneral().addSimbolo(sim);
        }
    }
    
    private static void buscarDeclaraciones(Nodo nodoSentencias, Metodo metodo, Ambito ambito)
    {
        for(Nodo nodo : nodoSentencias.getNodos())
        {
            switch(nodo.getNombre().toLowerCase())
            {
                case "declaracion_variable":
                    Declaracion.declaracionVariables(nodo, metodo, ambito);
                    break;
                case "arreglo":
                    Declaracion.declaracionArreglos(nodo, metodo, ambito);
                    break;
                case "instanciacion_struct":
                    Declaracion.declaracionEstructura(nodo, metodo, ambito);
                    break;
                case "sentencia_while":
                    buscarEnWhile(nodo, metodo, ambito);
                    break;
                case "sentencia_for":
                    buscarEnFor(nodo, metodo, ambito);
                    break;
                case "sentencia_if":
                    buscarEnIf(nodo, metodo, ambito);
                    break;
            }
        }
    }
    
    public static void buscarEnWhile(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        //Creando ambito
        Ambito nAmbito = new Ambito();
        nAmbito.setNombreAmbito(metodo.getNombre());
        //Copiando simbolos
        copiarSimbolos(ambito, nAmbito);
        //Agregando simbolos a la sentencia
        nodo.setAmbitoLocal(nAmbito);
        //Buscando declaraciones dentro de las sentencias
        buscarDeclaraciones(nodo.getNodo(1), metodo, nAmbito);
    }
    
    public static void buscarEnFor(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        //Creando ambito
        Ambito nAmbito = new Ambito();
        nAmbito.setNombreAmbito(metodo.getNombre());
        //Copiando simbolos
        copiarSimbolos(ambito, nAmbito);
        //Agregando simbolos a la sentencia
        nodo.setAmbitoLocal(nAmbito);
        //Buscando declaraciones dentro de las sentencias
        buscarDeclaraciones(nodo.getNodo(0), metodo, nAmbito);
        buscarDeclaraciones(nodo.getNodo(3), metodo, nAmbito);
    }
    
    public static void buscarEnIf(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        //Creando ambito
        Ambito nAmbito = new Ambito();
        nAmbito.setNombreAmbito(metodo.getNombre());
        //Copiando simbolos
        copiarSimbolos(ambito, nAmbito);
        //Agregando simbolos a la sentencia
        nodo.setAmbitoLocal(nAmbito);
        //Buscando declaraciones dentro de las sentencias
        buscarDeclaraciones(nodo.getNodo(1), metodo, nAmbito);
        
        Nodo nodosElif = null;
        Nodo nodoElse = null;
        
        for(int x=0; x<nodo.getNodos().size(); x++)
        {
            Nodo nodoIf = nodo.getNodo(x);
            if(nodoIf.getNombre().equals("sentencias_elif"))
            {
                nodosElif = nodoIf;
            }
            if(nodoIf.getNombre().equals("sentencia_else"))
            {
                nodoElse = nodoIf;
            }
        }
        
        if(nodosElif!=null)
        {
            for(Nodo nodoElif : nodosElif.getNodos())
            {
                buscarEnElse(nodoElif, metodo, nAmbito);
            }
        }
        
        //Los ambitos se guardan en el nodo de las sentencias
        if(nodoElse!=null)
        {
            buscarEnElse(nodoElse, metodo, nAmbito);
        }
    }
    
    public static void buscarEnElse(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        //Creando ambito
        Ambito nAmbito = new Ambito();
        nAmbito.setNombreAmbito(metodo.getNombre());
        //Copiando simbolos
        copiarSimbolos(ambito, nAmbito);
        //Agregando simbolos a la sentencia
        nodo.setAmbitoLocal(nAmbito);
        //Buscando declaraciones dentro de las sentencias
        buscarDeclaraciones(nodo, metodo, nAmbito);
    }
    
    private static void copiarSimbolos(Ambito a1, Ambito a2)
    {
        for(Simbolo sim : a1.getSimbolos())
        {
            a2.addSimbolo(sim);
        }
    }
}
