/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

/**
 *
 * @author rodolfo
 */
public class Ciclo {
    private String etiquetaSalida = "";
    private String etiquetaInicio = "";

    public String getEtiquetaSalida() {
        return etiquetaSalida;
    }

    public void setEtiquetaSalida(String etiquetaSalida) {
        this.etiquetaSalida = etiquetaSalida;
    }

    public String getEtiquetaInicio() {
        return etiquetaInicio;
    }

    public void setEtiquetaInicio(String etiquetaInicio) {
        this.etiquetaInicio = etiquetaInicio;
    }

}
