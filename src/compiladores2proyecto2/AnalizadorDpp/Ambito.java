/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class Ambito {
    private String nombreAmbito = "";
    private int tamano = 0;
    private LinkedList<Simbolo> simbolos = new LinkedList<Simbolo>();

    public LinkedList<Simbolo> getSimbolos() {
        return this.simbolos;
    }
    
    public void addSimbolo(Simbolo simbolo){
        this.simbolos.addLast(simbolo);
    }
    
    public Simbolo getSimbolo(String id){
        for(Simbolo simbolo : this.simbolos){
            if(simbolo.getNombre().equals(id)){
                return simbolo;
            }
        }
        return null;
    }

    public String getNombreAmbito() {
        return nombreAmbito;
    }

    public void setNombreAmbito(String nombreAmbito) {
        this.nombreAmbito = nombreAmbito;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }
    
    public void aumentarTamano(int numero){
        this.tamano += numero;
    }
}
