/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class SentenciaPara {
    
    public static void generarSentencia(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        
        Nodo nodoInicializacion = nodo.getNodo(0);
        Nodo nodoExpresion = nodo.getNodo(1);
        Nodo nodoActualizacion = nodo.getNodo(2);
        Nodo nodoSentencias = nodo.getNodo(3);
        
        String etiquetaInicio = genEtq();
        String etiquetaActualizacion = genEtq();
        String etiquetaSalida = "";
        
        
        escribir("//Sentencia PARA");
        
        escribir("//Inicializacion");
        GenerarMetodo.generarCodigoSentencias(nodoInicializacion, metodo, nodo.getAmbitoLocal());
        
        escribir("//Condicion");
        escribir(etiquetaInicio);
        Dato dato = Expresiones.nodoExpresion(nodoExpresion, metodo, nodo.getAmbitoLocal());
        escribir(dato.getValor());
        
        //Registrando en el display
        etiquetaSalida = dato.getEtqF();
        Ciclo ciclo = new Ciclo();
        ciclo.setEtiquetaInicio(etiquetaActualizacion);
        ciclo.setEtiquetaSalida(etiquetaSalida);
        Display.addCiclo(ciclo);
        
        escribir("//Etiqueta verdadera");
        escribir(dato.getEtqV());
        
        escribir("//Sentencias condicion verdadera");
        GenerarMetodo.generarCodigoSentencias(nodoSentencias, metodo, nodo.getAmbitoLocal());
        
        escribir("//Actualizacion");
        escribir(etiquetaActualizacion);
        GenerarMetodo.generarCodigoSentencias(nodoActualizacion, metodo, nodo.getAmbitoLocal());
        
        escribir("//Salto a inicio");
        escribir("Br " + etiquetaInicio);
        
        escribir("//Etiqueta condicion falsa");
        escribir(dato.getEtqF());
        
        Display.removeCiclo(ciclo);
    }
    
}
