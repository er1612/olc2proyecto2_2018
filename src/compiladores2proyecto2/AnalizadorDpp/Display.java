/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class Display {
    private static LinkedList<Ciclo> ciclos = new LinkedList<Ciclo>();
    
    public static void addCiclo(Ciclo cl)
    {
        ciclos.add(cl);
    }
    
    public static void removeCiclo(Ciclo cl)
    {
        ciclos.remove(cl);
    }

    public static LinkedList<Ciclo> getCiclos() {
        return ciclos;
    }

    public static void setCiclos(LinkedList<Ciclo> ciclos) {
        Display.ciclos = ciclos;
    }
    
    public static Ciclo getLastCiclo()
    {
        return ciclos.getLast();
    }
}
