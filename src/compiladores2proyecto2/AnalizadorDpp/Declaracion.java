/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Declaracion {
    
    public static void declaracionVariables(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String tipo = nodo.getTipo();
        
        for(Nodo variable : nodo.getNodos())
        {
            if(variableExiste(variable.getToken(), ambito))
            {
                errorVariableExiste(nodo, variable.getToken());
                continue;
            }
            
            Simbolo sim = new Simbolo();
            sim.setNombreAmbito(ambito.getNombreAmbito());
            sim.setNombre(variable.getToken());
            metodo.aumentarTamano(1);
            sim.setPosicion(metodo.getTamano());
            sim.setRol("variable");
            sim.setTamano(1);
            sim.setTipo(tipo);
            
            ambito.addSimbolo(sim);
            metodo.getAmbitoGeneral().addSimbolo(sim);
        }
    }
    
    public static void declaracionArreglos(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String tipo = nodo.getTipo();
        
        int dimensiones = nodo.getNodo(0).getNodos().size();
        
        String id = nodo.getToken();
        Simbolo sim = new Simbolo();
        sim.setDimensiones(dimensiones);
        sim.setNombre(id);
        sim.setNombreAmbito(metodo.getNombre());
        metodo.aumentarTamano(1);
        sim.setPosicion(metodo.getTamano());
        sim.setRol("arreglo");
        sim.setTamano(1);
        sim.setTipo(tipo);

        ambito.addSimbolo(sim);
        metodo.getAmbitoGeneral().addSimbolo(sim);
    }
    
    public static void declaracionEstructura(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String tipo = nodo.getTipo();
        
        String id = nodo.getToken();
        Simbolo sim = new Simbolo();
        sim.setDimensiones(0);
        sim.setNombre(id);
        sim.setNombreAmbito(metodo.getNombre());
        metodo.aumentarTamano(1);
        sim.setPosicion(metodo.getTamano());
        sim.setRol("estructura");
        sim.setTamano(1);
        sim.setTipo(tipo);

        ambito.addSimbolo(sim);
        metodo.getAmbitoGeneral().addSimbolo(sim);
    }
    
    public static boolean variableExiste(String id, Ambito ambito)
    {
        for(Simbolo sim : ambito.getSimbolos())
        {
            if(sim.getNombre().equals(id))
            {
                return true;
            }
        }
        return false;
    }
    
    public static void errorVariableExiste(Nodo operador, String id)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("La variable ya existe " + id + ".");
        Entorno.addError(error);
    }
}
