/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDasm.EjecutarDasm.vistaMem;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.ambitoGlobal;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribirEncabezado;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribirPie;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class GenerarMetodo {
    
    public static int generarCodigoMetodo(Metodo metodo)
    {
        escribirEncabezado(metodo.getNombre());
        
        if(metodo.getNombre().equals("principal"))
        {
            generarCodigoPrincipal();
        }
        
        if(metodo.getNombre().equals("global"))
        {
            generarCodigoGlobal();
        }
        
        generarCodigoSentencias(metodo.getSentencias(), metodo, metodo.getAmbitoPrincipal());
        
        escribirPie();
        return 0;
    }
    
    public static int generarCodigoSentencias(Nodo sentencias, Metodo metodo, Ambito ambito)
    {
        for(Nodo sentencia : sentencias.getNodos())
        {
            String nombreSentencia = sentencia.getNombre();
            
            switch(nombreSentencia)
            {
                case "declaracion_variable":
                    Asignacion.generarDeclaracionAsignacion(sentencia, metodo, ambito);
                    break;
                case "instanciacion_struct":
                    InstanciaEstructura.generarInstancia(sentencia, metodo, ambito);
                    break;
                case "arreglo":
                    InstanciaArreglo.generarInstancia(sentencia, metodo, ambito);
                    break;
                case "asignacion":
                    Asignacion.generarAsignacion(sentencia, metodo, ambito);
                    break;
                case "retornar":
                    Retorno.generarRetorno(sentencia, metodo, ambito);
                    break;
                case "sentencia_if":
                    SentenciaSi.generarSi(sentencia, metodo, ambito);
                    break;
                case "sentencia_while":
                    SentenciaMientras.generarSentencia(sentencia, metodo, ambito);
                    break;
                case "sentencia_for":
                    SentenciaPara.generarSentencia(sentencia, metodo, ambito);
                    break;
                case "detener":
                    SentenciaDetener.generarSentencia(sentencia);
                    break;
                case "continuar":
                    SentenciaContinuar.generarSentencia(sentencia);
                    break;
                case "aumento":
                    SentenciaAumento.generarSentencia(sentencia, metodo, ambito);
                    break;
                case "reduccion":
                    SentenciaReduccion.generarSentencia(sentencia, metodo, ambito);
                    break;
                case "print":
                    Imprimir.generarImprimir(sentencia, metodo, ambito);
                    break;
                case "llamado":
                    SentenciaLlamado.generarSentencia(sentencia, metodo, ambito);
                    break;
                case "punto":
                    FuncionesDibujo.generarPunto(sentencia, metodo, ambito);
                    break;
                case "rectangulo":
                    FuncionesDibujo.generarCuadrado(sentencia, metodo, ambito);
                    break;
                case "circulo":
                    FuncionesDibujo.generarOvalo(sentencia, metodo, ambito);
                    break;
                case "cadena":
                    FuncionesDibujo.generarCadena(sentencia, metodo, ambito);
                    break;
                case "linea":
                    FuncionesDibujo.generarLinea(sentencia, metodo, ambito);
                    break;
            }
            
            if(Entorno.esDepuracion)
            {
                Entorno.pintarLinea(sentencia.getArchivo(), sentencia.getFila());
            }
        }
        return 0;
    }
    
    public static void generarCodigoPrincipal()
    {
        escribir("//Inicializando variables globales");
        escribir("Call $global");
        escribir("");
        
        escribir("//Posicion inicial de Stack");
        if(ambitoGlobal.getTamano()==0)
        {
            escribir("1");
        }
        else
        {
            escribir(String.valueOf(ambitoGlobal.getTamano()));
        }
        escribir("Set_local 0");

        escribir("");
    }
    
    public static void generarCodigoGlobal()
    {
        escribir("//Posicion inicial de Stack");
        escribir("0");
        escribir("Set_local 0");
        
        escribir("//Posicion inicial de Heap");
        escribir("1");
        escribir("Set_global 0");
    }
}
