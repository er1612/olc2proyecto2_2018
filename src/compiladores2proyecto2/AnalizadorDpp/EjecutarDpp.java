/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import compiladores2proyecto2.AnalizadorDasm.lex_dasm;
import compiladores2proyecto2.AnalizadorDasm.sin_dasm;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.VentanaPrincipal;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author rodolfo
 */
public class EjecutarDpp {
    
    public static LinkedList<Metodo> metodos;
    public static LinkedList<Metodo> estructuras;
    public static Metodo ambitoGlobal;
    public static String codigoDasm = "";
    public static int etiqueta;
    public static String codigoConstructores = "";
    public static int archivosAnalizados = 0;
    public static Nodo nodoRaiz = null;
    public static LinkedList<String> listaArchivos = new LinkedList<String>();
    
    public static void Ejecutar(Nodo raiz)
    {
        if(archivosAnalizados == 0)
        {
            nodoRaiz = raiz;
            archivosAnalizados++;
            buscarImportar();
            EjecutarDpp();
        }
        else
        {
            for(Nodo nodo : raiz.getNodos())
            {
                nodoRaiz.addNodo(nodo);
            }
        }
    }
    
    public static void buscarImportar()
    {
        int x = 0;
        Nodo nodo = nodoRaiz.getNodo(x);
        
        while(nodo!=null)
        {
            if(nodo.getNombre().equalsIgnoreCase("importar"))
            {
                try {
                    ejecutarImportar(nodo);
                } catch (IOException ex) {
                    Logger.getLogger(EjecutarDpp.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            x++;
            nodo = nodoRaiz.getNodo(x);
        }
    }
    
    public static void ejecutarImportar(Nodo nodo) throws FileNotFoundException, IOException
    {
        System.out.println(archivosAnalizados);
        String nom = nodo.getToken().replace("\"", "");
        String rut = Entorno.tabEjecucion.getRutaArchivo();
        String rutnom = rut + "/" + nom;
        
        System.out.println(rutnom);
        
        File archivo = new File(rutnom);
            
        if(archivo.exists() && !archivo.isDirectory()) { 
            System.out.println(rutnom);
            BufferedReader br = null;

            br = new BufferedReader(new FileReader(rutnom));

            StringBuilder sb = new StringBuilder();

            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }   

            String texto = sb.toString();
            String nombreArchivo = archivo.getName();
            String rutaArchivo = archivo.getParent();
            String tipoArchivo = getExtension(archivo);
            Entorno.ventanaPrincipal.crearTab(nombreArchivo, rutaArchivo, tipoArchivo, texto);
            Entorno.setArchivo(nombreArchivo);
            analizarDpp(texto);
        }
    }
    
    private static void analizarDpp(String texto){
        try {
            new sin_dpp(new lex_dpp(new StringReader(texto))).parse();
        } catch (Exception ex) {
            Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static String getExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
    
    public static void EjecutarDpp()
    {
        metodos = new LinkedList<Metodo>();
        estructuras = new LinkedList<Metodo>();
        ambitoGlobal = new Metodo();
        ambitoGlobal.setNombre("global");
        codigoDasm = "";
        codigoConstructores = "";
        etiqueta = 0;
        Entorno.getVentanaPrincipal().textoDasm.setText("");
        
        if(nodoRaiz!=null)
        {
            crearTablaSimbolos(nodoRaiz);
            generarCodigoDasm();
            GuardarArchivoGenerado();
        }
    }

    //Creando la tabla de simbolos -----------------------------
    //----------------------------------------------------------
    private static void crearTablaSimbolos(Nodo raiz)
    {
        Metodo metodo = ambitoGlobal;
        metodo.getAmbitoGeneral().setNombreAmbito("global");
        metodo.getAmbitoPrincipal().setNombreAmbito("global");
        metodo.setSentencias(raiz);
        
        for(Nodo nodo : raiz.getNodos())
        {
            switch(nodo.getNombre().toLowerCase())
            {
                case "estructura":
                    DeclaracionEstructura.generarEstructura(nodo);
                    break;
                case "metodo":
                    DeclaracionMetodo.ejecutarDeclaracionMetodo(nodo);
                    break;
                case "declaracion_variable":
                    Declaracion.declaracionVariables(nodo, metodo, metodo.getAmbitoPrincipal());
                    break;
                case "instanciacion_struct":
                    Declaracion.declaracionEstructura(nodo, metodo, metodo.getAmbitoPrincipal());
                    break;
                case "arreglo":
                    Declaracion.declaracionArreglos(nodo, metodo, metodo.getAmbitoPrincipal());
                    break;
            }
        }
        
        mostrarTablaSimbolos();
    }
    
    private static void mostrarTablaSimbolos()
    {
        DefaultTableModel dm = new DefaultTableModel(0, 0);
        String header[] = new String[] { "Ambito", "Nombre", "Tipo",
            "Posicion", "Tamano", "Rol", "Params/Dims"};
        dm.setColumnIdentifiers(header);
        
        JTable tabla = Entorno.getVentanaPrincipal().tableSimbolos;
        
        tabla.setModel(dm);
        
        for(Metodo metodo : estructuras)
        {
            dm.addRow(filaVacia());
            
            Vector<Object> data = new Vector<Object>();
            data.add("Global");
            data.add(metodo.getNombre());
            data.add(metodo.getTipo());
            data.add("---------------");
            data.add(metodo.getTamano());
            data.add("estructura");
            data.add(metodo.getCantParametros());
            dm.addRow(data);

            for(Simbolo sim : metodo.getAmbitoGeneral().getSimbolos())
            {
                Vector<Object> data0 = new Vector<Object>();
                data0.add(sim.getNombreAmbito());
                data0.add(sim.getNombre());
                data0.add(sim.getTipo());
                data0.add(sim.getPosicion());
                data0.add(sim.getTamano());
                data0.add(sim.getRol());
                data0.add(sim.getDimensiones());
                dm.addRow(data0);
            }

        }
        
        dm.addRow(filaVacia());
        
        for(Simbolo sim : ambitoGlobal.getAmbitoGeneral().getSimbolos())
        {
            Vector<Object> data = new Vector<Object>();
            data.add(sim.getNombreAmbito());
            data.add(sim.getNombre());
            data.add(sim.getTipo());
            data.add(sim.getPosicion());
            data.add(sim.getTamano());
            data.add(sim.getRol());
            data.add(sim.getDimensiones());
            dm.addRow(data);
        }
        
        for(Metodo metodo : metodos)
        {
            dm.addRow(filaVacia());
            
            Vector<Object> data = new Vector<Object>();
            data.add("Global");
            data.add(metodo.getNombre());
            data.add(metodo.getTipo());
            data.add("---------------");
            data.add(metodo.getTamano());
            data.add("metodo");
            data.add(metodo.getCantParametros());
            dm.addRow(data);

            for(Simbolo sim : metodo.getAmbitoGeneral().getSimbolos())
            {
                Vector<Object> data0 = new Vector<Object>();
                data0.add(sim.getNombreAmbito());
                data0.add(sim.getNombre());
                data0.add(sim.getTipo());
                data0.add(sim.getPosicion());
                data0.add(sim.getTamano());
                data0.add(sim.getRol());
                data0.add(sim.getDimensiones());
                dm.addRow(data0);
            }
        }
        
        ajustarColumnas(tabla);
        
    }
    
    private static Vector<Object> filaVacia()
    {
        Vector<Object> data = new Vector<Object>();
        data.add("");
        data.add("");
        data.add("");
        data.add("");
        data.add("");
        data.add("");
        data.add("");
        return data;
    }
    
    private static void ajustarColumnas(JTable table)
    {
        
        final TableColumnModel columnModel = table.getColumnModel();
        
        for (int column = 0; column < table.getColumnCount(); column++)
        {
            int width = 100;
            for (int row = 0; row < table.getRowCount(); row++)
            {
                String value = table.getModel().getValueAt(row, column).toString();
                int temp = value.length() * 8;
                
                if(temp>width)
                {
                    width = temp;
                }
            }
            columnModel.getColumn(column).setPreferredWidth(width);
        }
    }
    
    //Metodos para iniciar generacion de codigo ----------------
    //----------------------------------------------------------
    
    private static void generarCodigoDasm()
    {
        GenerarMetodo.generarCodigoMetodo(ambitoGlobal);
        
        for(Metodo metodo : metodos)
        {
            GenerarMetodo.generarCodigoMetodo(metodo);
        }
        
        String funciones = agregarFunciones();
        codigoDasm += funciones;
        Entorno.getVentanaPrincipal().textoDasm.append(funciones);
        codigoDasm += codigoConstructores;
        Entorno.getVentanaPrincipal().textoDasm.append(codigoConstructores);
        
        
    }
    
    private static void GuardarArchivoGenerado()
    {
        String texto = codigoDasm;
        JFileChooser fc = new JFileChooser();
            
        int returnVal = fc.showSaveDialog(Entorno.ventanaPrincipal);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File file = fc.getSelectedFile();

            try {
                PrintStream out = new PrintStream(new FileOutputStream(file));
                out.print(texto);
                out.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(VentanaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static String agregarFunciones()
    {
        try {
            BufferedReader br = new BufferedReader(new FileReader("/home/rodolfo/Escritorio/Auxiliares.txt"));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            
            br.close();
            
            return sb.toString();
        } catch (IOException ex) {
            Logger.getLogger(EjecutarDpp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return "";
    }
    
    public static String genEtq()
    {
        String etq = "$L" + etiqueta;
        etiqueta++;
        return etq;
    }
    
    //Metodos para escritura de codigo -------------------------
    //----------------------------------------------------------
    public static void escribir(String codigo)
    {
        String code = codigo + "\n";
        codigoDasm += code;
        Entorno.getVentanaPrincipal().textoDasm.append(code);
    }
    
    public static void escribirEncabezado(String codigo)
    {
        String code = "Function $" + codigo + "\n\n";
        codigoDasm += code;
        Entorno.getVentanaPrincipal().textoDasm.append(code);
    }
    
    public static void escribirPie()
    {
        String code = "\nEnd\n";
        codigoDasm += code;
        Entorno.getVentanaPrincipal().textoDasm.append(code);
    }
}
