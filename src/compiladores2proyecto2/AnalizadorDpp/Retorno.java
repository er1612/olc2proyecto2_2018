/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class Retorno {
    
    public static void generarRetorno(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        escribir("//Retorno");
        if(nodo.getNodos().size()>0)
        {
            Dato expresion = Expresiones.nodoExpresion(nodo.getNodo(0), metodo, ambito);
            escribir(expresion.getValor());
            
            if(expresion.getTipo().equalsIgnoreCase("booleano"))
            {
                String etqSalida = genEtq();

                escribir(expresion.getEtqV());
                escribir("1");
                escribir("Br " + etqSalida);
                escribir(expresion.getEtqF());
                escribir("0");
                escribir(etqSalida);
            }
            
            escribir("Get_local 0");
            escribir("Set_local $calc");
        }
        escribir("$ret");
    }
    
}
