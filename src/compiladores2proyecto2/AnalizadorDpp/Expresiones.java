/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Expresiones {
    
    public static Dato nodoExpresion(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String nombreExpresion = nodo.getNombre().toLowerCase();
        
        switch(nombreExpresion)
        {
            case "dato":
            case "acceso":
                return getDato(nodo, metodo, ambito);
            case "expresion_binaria":
                return getResultadoBinario(nodo, metodo, ambito);
            case "expresion_unaria_pre":
                return getResultadoUnarioPre(nodo, metodo, ambito);
            case "expresion_unaria_post":
            case "aumento":
            case "reduccion":
                return getResultadoUnarioPost(nodo, metodo, ambito);
            case "seleccion":
                return SentenciaSeleccion.generarSentencia(nodo, metodo, ambito);
        }

        return getDatoNulo();
    }
    
    public static Dato getResultadoBinario(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String operador = nodo.getToken();
        
        if(operador.equals("+"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getSuma(nodo, val1, val2, metodo);
        }
        else if(operador.equals("-"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getResta(nodo, val1, val2, metodo);
        }
        else if(operador.equals("*"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getMultiplicacion(nodo, val1, val2, metodo);
        }
        else if(operador.equals("/"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getDivision(nodo, val1, val2, metodo);
        }
        else if(operador.equals("^"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getPotencia(nodo, val1, val2, metodo);
        }
        else if(operador.equals("<"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getMenor(nodo, val1, val2, metodo);
        }
        else if(operador.equals(">"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getMayor(nodo, val1, val2, metodo);
        }
        else if(operador.equals("<="))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getMenorIgual(nodo, val1, val2, metodo);
        }
        else if(operador.equals(">="))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getMayorIgual(nodo, val1, val2, metodo);
        }
        else if(operador.equals("=="))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getIgual(nodo, val1, val2, metodo);
        }
        else if(operador.equals("<>"))
        {
            Dato val1 = nodoExpresion(nodo.getNodo(0), metodo, ambito);
            Dato val2 = nodoExpresion(nodo.getNodo(1), metodo, ambito);
            return getDiferente(nodo, val1, val2, metodo);
        }
        else if(operador.equals("&&"))
        {
            return getAnd(nodo, nodo.getNodo(0), nodo.getNodo(1), metodo, ambito);
        }
        else if(operador.equals("||"))
        {
            return getOr(nodo, nodo.getNodo(0), nodo.getNodo(1), metodo, ambito);
        }
        
        return getDatoNulo();
    }
    
    public static Dato getResultadoUnarioPre(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String operador = nodo.getToken();
        
        Dato val = nodoExpresion(nodo.getNodo(0), metodo, ambito);
        
        switch(operador)
        {
            case "!":
                return getNot(nodo, val);
            case "+":
                return getPositivo(nodo, val);
            case "-":
                return getNegativo(nodo, val);
        }
        return getDatoNulo();
    }
    
    public static Dato getResultadoUnarioPost(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String operador = nodo.getToken();
        
        Dato val = nodoExpresion(nodo.getNodo(0), metodo, ambito);
        
        switch(operador)
        {
            case "++":
                return getAumento(nodo, val);
            case "--":
                return getReduccion(nodo, val);
        }
        return getDatoNulo();
    }
    
    public static Dato getDato(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        if(nodo.getNombre().equalsIgnoreCase("acceso"))
        {
            return ExpresionAcceso.ejecutarAcceso(nodo, metodo, ambito);
        }
        else if(nodo.getTipo().equalsIgnoreCase("nulo"))
        {
            Dato dato = new Dato();
            dato.setTipo("tiponulo");
            dato.concat("-1");
            return dato;
        }
        else if(nodo.getTipo().equalsIgnoreCase("cadena"))
        {
            String cadena = nodo.getToken();
            String tipo = nodo.getTipo();
            Dato dato = new Dato();
            dato.setTipo(tipo);
            
            dato.concat("//Cadena " + cadena);
            dato.concat("Get_global 0");
            
            for(char caracter : cadena.toCharArray())
            {
                String valor = String.valueOf((int)caracter);
                
                dato.concat(valor);
                dato.concat("Get_global 0");
                dato.concat("Set_global $calc");
                
                dato.concat("Get_global 0");
                dato.concat("1");
                dato.concat("Add");
                dato.concat("Set_global 0");
            }
            
            dato.concat("0");
            dato.concat("Get_global 0");
            dato.concat("Set_global $calc");
            
            dato.concat("Get_global 0");
            dato.concat("1");
            dato.concat("Add");
            dato.concat("Set_global 0");
            
            return dato;
        }
        else if(nodo.getTipo().equalsIgnoreCase("caracter"))
        {
            int caracter = (int)nodo.getToken().charAt(0);
            Dato dato = new Dato();
            dato.setTipo(nodo.getTipo());
            
            dato.concat(String.valueOf(caracter));
            
            return dato;
        }
        else if(nodo.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo(nodo.getTipo());
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(nodo.getToken());
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            Dato dato = new Dato();
            dato.setTipo(nodo.getTipo());
            
            dato.concat(nodo.getToken());
            
            return dato;
        }
    }
    
    public static Dato getDatoNulo()
    {
        Dato dato = new Dato();
        dato.setTipo("nulo");
        dato.setValor("");
        return dato;
    }
    
    //Operaciones ARITMETICAS----------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    public static Dato getSuma(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
            val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
            val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Add");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Add");
            
            return dato;
            
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Add");
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Add");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Add");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Add");
            
            return dato;
        }
        //Resultados de tipo CADENA
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.setValor(val1.getValor() + val2.getValor());
            concatString(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.setValor(val1.getValor());
            intToString(dato, metodo);
            
            dato.concat(val2.getValor());
            concatString(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.setValor(val1.getValor());
            
            dato.concat(val2.getValor());
            intToString(dato, metodo);
            
            concatString(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.setValor(val1.getValor());
            decimalToString(dato, metodo);
            
            dato.concat(val2.getValor());
            concatString(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.setValor(val1.getValor());
            
            dato.concat(val2.getValor());
            decimalToString(dato, metodo);
            
            concatString(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.concat("Get_global 0");
                
            dato.concat(val1.getValor());
            
            dato.concat("Get_global 0");
            dato.concat("Set_global $calc");

            dato.concat("Get_global 0");
            dato.concat("1");
            dato.concat("Add");
            dato.concat("Set_global 0");
            
            dato.concat("0");
            dato.concat("Get_global 0");
            dato.concat("Set_global $calc");
            
            dato.concat("Get_global 0");
            dato.concat("1");
            dato.concat("Add");
            dato.concat("Set_global 0");
            
            dato.concat(val2.getValor());
            
            concatString(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("caracter"))
        {
            Dato dato = new Dato();
            dato.setTipo("cadena");
            
            dato.concat(val1.getValor());
            
            dato.concat("Get_global 0");
                
            dato.concat(val2.getValor());
            
            dato.concat("Get_global 0");
            dato.concat("Set_global $calc");

            dato.concat("Get_global 0");
            dato.concat("1");
            dato.concat("Add");
            dato.concat("Set_global 0");
            
            dato.concat("0");
            dato.concat("Get_global 0");
            dato.concat("Set_global $calc");
            
            dato.concat("Get_global 0");
            dato.concat("1");
            dato.concat("Add");
            dato.concat("Set_global 0");
            
            concatString(dato, metodo);
            
            return dato;
        }
        else
        {
            reportarErrorTipos("SUMA", operador, val1, val2);
        }
        
        return getDatoNulo();
    }
    
    public static void concatString(Dato dato, Metodo metodo)
    {
        dato.concat("//Parametro 2");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("2");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Parametro 1");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("1");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("Set_local 0");
        dato.concat("Call $concatString");
        dato.concat("//Obteniendo valor de retorno");
        dato.concat("Get_local 0");
        dato.concat("Get_local $calc");
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Diff");
        dato.concat("Set_local 0");
    }
    
    public static void decimalToString(Dato dato, Metodo metodo)
    {
        dato.concat("//Parametro 1");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("1");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("Set_local 0");
        dato.concat("Call $decimalToString");
        dato.concat("//Obteniendo valor de retorno");
        dato.concat("Get_local 0");
        dato.concat("Get_local $calc");
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Diff");
        dato.concat("Set_local 0");
    }
    
    public static void intToString(Dato dato, Metodo metodo)
    {
        dato.concat("//Parametro 1");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("1");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("Set_local 0");
        dato.concat("Call $intToString");
        dato.concat("//Obteniendo valor de retorno");
        dato.concat("Get_local 0");
        dato.concat("Get_local $calc");
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Diff");
        dato.concat("Set_local 0");
    }
    
    public static Dato getResta(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
            val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
            val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Diff");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Diff");
            
            return dato;
            
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Diff");
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Diff");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Diff");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Diff");
            
            return dato;
        }
        else
        {
            reportarErrorTipos("RESTA", operador, val1, val2);
        }
        
        return getDatoNulo();
    }
    
    public static Dato getMultiplicacion(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
            val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
            val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Mult");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Mult");
            
            return dato;
            
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Mult");
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Mult");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Mult");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Mult");
            
            return dato;
        }
        else
        {
            reportarErrorTipos("MULTIPLICACION", operador, val1, val2);
        }
        
        return getDatoNulo();
    }
    
    public static Dato getDivision(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
            val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
            val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Div");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Div");
            
            return dato;
            
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Div");
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            dato.concat("Div");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            dato.concat("Div");
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat("Div");
            
            return dato;
        }
        else
        {
            reportarErrorTipos("DIVISION", operador, val1, val2);
        }
        
        return getDatoNulo();
    }
    
    public static Dato getPotencia(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        //Resultados de tipo DECIMAL
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
            val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
            val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
            val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            
            pow(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            pow(dato, metodo);
            
            return dato;
            
        }
        else if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            pow(dato, metodo);
            
            return dato;
        }
        //Resultados de tipo ENTERO
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.setValor( val1.getValor() + val2.getValor() );
            
            pow(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("booleano") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            dato.concat(val1.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val1.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            dato.concat(val2.getValor());
            
            pow(dato, metodo);
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("booleano"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            String etqSalida = genEtq();
            
            dato.concat(val1.getValor());
            
            dato.concat(val2.getValor());
            dato.concat(val2.getEtqV());
            dato.concat("1");
            dato.concat("Br " + etqSalida);
            dato.concat(val2.getEtqF());
            dato.concat("0");
            dato.concat(etqSalida);
            
            pow(dato, metodo);
            
            return dato;
        }
        else
        {
            reportarErrorTipos("POTENCIA", operador, val1, val2);
        }
        
        return getDatoNulo();
    }
    
    public static void pow(Dato dato, Metodo metodo)
    {
        dato.concat("//Parametro 2");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("3");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Parametro 1");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("1");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("Set_local 0");
        dato.concat("Call $pow");
        dato.concat("//Obteniendo valor de retorno");
        dato.concat("Get_local 0");
        dato.concat("Get_local $calc");
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Diff");
        dato.concat("Set_local 0");
    }
    
    public static Dato getPositivo(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            
            dato.concat(val.getValor());
            dato.concat("1");
            dato.concat("Mult");
            
            return dato;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.concat(val.getValor());
            dato.concat("1");
            dato.concat("Mult");
            
            return dato;
        }
        else
        {
            reportarErrorTipos("POSITIVO ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getNegativo(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("entero");
            
            dato.concat(val.getValor());
            dato.concat("-1");
            dato.concat("Mult");
            
            return dato;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            Dato dato = new Dato();
            dato.setTipo("decimal");
            
            dato.concat(val.getValor());
            dato.concat("-1");
            dato.concat("Mult");
            
            return dato;
        }
        else
        {
            reportarErrorTipos("NEGATIVO ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getAumento(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            val.concat("1");
            val.concat("Add");
            
            return val;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            val.concat("1");
            val.concat("Add");
            
            return val;
        }
        else
        {
            reportarErrorTipos("AUMENTO (++) ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    public static Dato getReduccion(Nodo operador, Dato val)
    {
        if(val.getTipo().equalsIgnoreCase("entero"))
        {
            val.concat("1");
            val.concat("Diff");
            
            return val;
        }
        else if(val.getTipo().equalsIgnoreCase("decimal"))
        {
            val.concat("1");
            val.concat("Diff");
            
            return val;
        }
        else
        {
            reportarErrorTipos("REDUCCION (--) ", operador, val, null);
        }
        return getDatoNulo();
    }
    
    //Operaciones RELACIONALES---------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    public static void stringSize(Dato dato, Metodo metodo)
    {
        dato.concat("//Parametro 1");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("1");
        dato.concat("Add");
        dato.concat("Set_local $calc");

        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Add");
        dato.concat("Set_local 0");
        dato.concat("Call $stringSize");
        dato.concat("//Obteniendo valor de retorno");
        dato.concat("Get_local 0");
        dato.concat("Get_local $calc");
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(metodo.getTamano() + 1));
        dato.concat("Diff");
        dato.concat("Set_local 0");
    }
    
    public static Dato getMayor(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor() + val2.getValor());
            
            dato.concat("Gt");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor());
            stringSize(dato, metodo);
            
            dato.concat(val2.getValor());
            stringSize(dato, metodo);
            
            dato.concat("Gt");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MAYOR (>)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMenor(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor() + val2.getValor());
            
            dato.concat("Lt");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor());
            stringSize(dato, metodo);
            
            dato.concat(val2.getValor());
            stringSize(dato, metodo);
            
            dato.concat("Lt");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MENOR (<)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMayorIgual(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor() + val2.getValor());
            
            dato.concat("Gte");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor());
            stringSize(dato, metodo);
            
            dato.concat(val2.getValor());
            stringSize(dato, metodo);
            
            dato.concat("Gte");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MAYOR O IGUAL (>=)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getMenorIgual(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor() + val2.getValor());
            
            dato.concat("Lte");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor());
            stringSize(dato, metodo);
            
            dato.concat(val2.getValor());
            stringSize(dato, metodo);
            
            dato.concat("Lte");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION MENOR O IGUAL (<=)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getIgual(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor() + val2.getValor());
            
            dato.concat("Diff");
            dato.concat("Eqz");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor());
            stringSize(dato, metodo);
            
            dato.concat(val2.getValor());
            stringSize(dato, metodo);
            
            dato.concat("Diff");
            dato.concat("Eqz");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION IGUAL (==)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    public static Dato getDiferente(Nodo operador, Dato val1, Dato val2, Metodo metodo)
    {
        if(val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("entero")||
                val1.getTipo().equalsIgnoreCase("decimal") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("entero") && val2.getTipo().equalsIgnoreCase("caracter")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("decimal")||
                val1.getTipo().equalsIgnoreCase("caracter") && val2.getTipo().equalsIgnoreCase("entero"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor() + val2.getValor());
            
            dato.concat("Diff");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else if(val1.getTipo().equalsIgnoreCase("cadena") && val2.getTipo().equalsIgnoreCase("cadena"))
        {
            Dato dato = new Dato();
            dato.setTipo("booleano");
            
            dato.addEtqFalso(genEtq());
            dato.addEtqVerdadero(genEtq());
            
            dato.concat(val1.getValor());
            stringSize(dato, metodo);
            
            dato.concat(val2.getValor());
            stringSize(dato, metodo);
            
            dato.concat("Diff");
            dato.concat("Br_if " + dato.getEtqF());
            dato.concat("Br " + dato.getEtqV());
            
            return dato;
        }
        else
        {
            reportarErrorTipos("COMPARACION DIFERENTE (<>)", operador, val1, val2);
        }
        return getDatoNulo();
    }
    
    //Operaciones RELACIONALES---------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    
    public static Dato getAnd(Nodo operador, Nodo v1, Nodo v2, Metodo metodo, Ambito ambito)
    {
        Dato dato = new Dato();
        dato.setTipo("booleano");
        
        Dato val1 = nodoExpresion(v1, metodo, ambito);
        
        if(!val1.getTipo().equalsIgnoreCase("booleano"))
        {
            reportarErrorTipos("LOGICA AND (&&)", operador, val1, getDatoNulo());
            return getDatoNulo();
        }
        
        dato.concat(val1.getValor());
        dato.concat(val1.getEtqV());
        
        Dato val2 = nodoExpresion(v2, metodo, ambito);
        
        if(!val2.getTipo().equalsIgnoreCase("booleano"))
        {
            reportarErrorTipos("LOGICA AND (&&)", operador, val1, val2);
            return getDatoNulo();
        }
        dato.concat(val2.getValor());
        dato.addEtqVerdadero(val2.getEtqV());
        dato.addEtqFalso(val1.getEtqF() + "\n" + val2.getEtqF());
        
        return dato;
    }
    
    public static Dato getOr(Nodo operador, Nodo v1, Nodo v2, Metodo metodo, Ambito ambito)
    {
        Dato dato = new Dato();
        dato.setTipo("booleano");
        
        Dato val1 = nodoExpresion(v1, metodo, ambito);
        
        if(!val1.getTipo().equalsIgnoreCase("booleano"))
        {
            reportarErrorTipos("LOGICA OR (||)", operador, val1, getDatoNulo());
            return getDatoNulo();
        }
        
        dato.concat(val1.getValor());
        dato.concat(val1.getEtqF());
        
        Dato val2 = nodoExpresion(v2, metodo, ambito);
        
        if(!val2.getTipo().equalsIgnoreCase("booleano"))
        {
            reportarErrorTipos("LOGICA OR (||)", operador, val1, val2);
            return getDatoNulo();
        }
        
        dato.concat(val2.getValor());
        dato.addEtqVerdadero(val1.getEtqV() + "\n" + val2.getEtqV());
        dato.addEtqFalso(val2.getEtqF());
        
        return dato;
    }
    
    public static Dato getNot(Nodo operador, Dato val1)
    {
        if(!val1.getTipo().equalsIgnoreCase("booleano"))
        {
            reportarErrorTipos("LOGICA NOT (!)", operador, val1, null);
            return getDatoNulo();
        }
        
        Dato dato = new Dato();
        dato.setTipo("booleano");
        dato.concat(val1.getValor());
        dato.addEtqVerdadero(val1.getEtqF());
        dato.addEtqFalso(val1.getEtqV());
        
        return dato;
    }
    
    //Errores de tipo y operacion------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------
    public static void reportarErrorTipos(String operacion, Nodo operador, Dato val1, Dato val2)
    {
        String tipoVal2 = "";
        
        if(val2!=null)
        {
            tipoVal2 =  " y " + val2.getTipo();
        }
        
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se puede hacer la operacion " + operacion + " con valores de tipo " + 
                val1.getTipo() + tipoVal2 + ".");
        Entorno.addError(error);
    }
}
