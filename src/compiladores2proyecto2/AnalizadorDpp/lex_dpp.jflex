package compiladores2proyecto2.AnalizadorDpp;
import java_cup.runtime.Symbol;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.Entorno;
import java.awt.Color;
import compiladores2proyecto2.RegistroError;
%%
%cupsym sim_dpp
%class lex_dpp
%cup
%public
%unicode
%line
%char
%column

%{
    public static String archivo = "";

	public void PrintToken(String str){
		System.out.println(str);
	}
%}

entero 				=	([0-9])+
ndecimal			=	([0-9])+("."([0-9])+)
cadena				=	"\"" ([^\"])* "\""
caracter			=	"'" (.) "'"
comentario 			=	"//" (.)* "\n"
comentario_multi	=	"/*" ([^])* "*/"
identificador		=	([a-zA-Z]|"_")([a-zA-Z]|[0-9]|"_")*

%%
{entero}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#721783"));
	return new Symbol(sim_dpp.entero, yycolumn,yyline,new String(yytext()));
}

{ndecimal}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#721783"));
	return new Symbol(sim_dpp.ndecimal, yycolumn,yyline,new String(yytext()));
}

{cadena}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#e17d00"));
	return new Symbol(sim_dpp.cadena, yycolumn,yyline,new String(yytext()));
}

{caracter}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#e17d00"));
	return new Symbol(sim_dpp.caracter, yycolumn,yyline,new String(yytext()));
}

//Expresiones Booleanas----------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"verdadero"
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#151e96"));
	return new Symbol(sim_dpp.rtrue, yycolumn,yyline,new String(yytext()));
}

"falso"
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#151e96"));
	return new Symbol(sim_dpp.rfalse, yycolumn,yyline,new String(yytext()));
}

//Operadores Aritmeticos---------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"+"					{return new Symbol(sim_dpp.mas, yycolumn,yyline,new String(yytext()));}
"-"					{return new Symbol(sim_dpp.menos, yycolumn,yyline,new String(yytext()));}
"*"					{return new Symbol(sim_dpp.por, yycolumn,yyline,new String(yytext()));}
"/"					{return new Symbol(sim_dpp.dividido, yycolumn,yyline,new String(yytext()));}
"%"					{return new Symbol(sim_dpp.modulo, yycolumn,yyline,new String(yytext()));}
"^"					{return new Symbol(sim_dpp.potencia, yycolumn,yyline,new String(yytext()));}
"++"				{return new Symbol(sim_dpp.adicion, yycolumn,yyline,new String(yytext()));}
"--"				{return new Symbol(sim_dpp.sustraccion, yycolumn,yyline,new String(yytext()));}
//Signos de Agrupacion-----------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"("					{return new Symbol(sim_dpp.a_parentesis, yycolumn,yyline,new String(yytext()));}
")"					{return new Symbol(sim_dpp.c_parentesis, yycolumn,yyline,new String(yytext()));}
//Operadores Realcionales--------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
">"					{return new Symbol(sim_dpp.mayor, yycolumn,yyline,new String(yytext()));}
"<"					{return new Symbol(sim_dpp.menor, yycolumn,yyline,new String(yytext()));}
">="				{return new Symbol(sim_dpp.mayor_igual, yycolumn,yyline,new String(yytext()));}
"<="				{return new Symbol(sim_dpp.menor_igual, yycolumn,yyline,new String(yytext()));}
"=="				{return new Symbol(sim_dpp.igual_relacional, yycolumn,yyline,new String(yytext()));}
"<>"				{return new Symbol(sim_dpp.diferente, yycolumn,yyline,new String(yytext()));}
//Operadores Logicos-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"&&"				{return new Symbol(sim_dpp.and, yycolumn,yyline,new String(yytext()));}
"||"				{return new Symbol(sim_dpp.or, yycolumn,yyline,new String(yytext()));}
"!"					{return new Symbol(sim_dpp.not, yycolumn,yyline,new String(yytext()));}
//Declaracion--------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
("vacio"|"entero"|"decimal"|"cadena"|"caracter"|"booleano")
{
	return new Symbol(sim_dpp.tipo, yycolumn,yyline,new String(yytext()));
}

"="					{return new Symbol(sim_dpp.igual, yycolumn,yyline,new String(yytext()));}
//Sentencia If-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"si"				{return new Symbol(sim_dpp.rif, yycolumn,yyline,new String(yytext()));}
"sino"				{return new Symbol(sim_dpp.ifnot, yycolumn,yyline,new String(yytext()));}
//Ciclos-------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"detener"			{return new Symbol(sim_dpp.smash, yycolumn,yyline,new String(yytext()));}
"continuar"			{return new Symbol(sim_dpp.continuar, yycolumn,yyline,new String(yytext()));}

"mientras"			{return new Symbol(sim_dpp.rwhile, yycolumn,yyline,new String(yytext()));}
"para"				{return new Symbol(sim_dpp.rfor, yycolumn,yyline,new String(yytext()));}
//Palabras reservadas------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
"imprimir"			{return new Symbol(sim_dpp.rprint, yycolumn,yyline,new String(yytext()));}
"punto"				{return new Symbol(sim_dpp.point, yycolumn,yyline,new String(yytext()));}
"cuadrado"			{return new Symbol(sim_dpp.quadrate, yycolumn,yyline,new String(yytext()));}
"ovalo"				{return new Symbol(sim_dpp.oval, yycolumn,yyline,new String(yytext()));}
"linea"				{return new Symbol(sim_dpp.rline, yycolumn,yyline,new String(yytext()));}
"estructura"		{return new Symbol(sim_dpp.estructura, yycolumn,yyline,new String(yytext()));}
"retornar"			{return new Symbol(sim_dpp.retornar, yycolumn,yyline,new String(yytext()));}
"importar"			{return new Symbol(sim_dpp.importar, yycolumn,yyline,new String(yytext()));}
"nulo"				{return new Symbol(sim_dpp.nulo, yycolumn,yyline,new String(yytext()));}
//No son del lenguaje------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
";"					{return new Symbol(sim_dpp.puntocoma, yycolumn,yyline,new String(yytext()));}
"{"					{return new Symbol(sim_dpp.a_llave, yycolumn,yyline,new String(yytext()));}
"}"					{return new Symbol(sim_dpp.c_llave, yycolumn,yyline,new String(yytext()));}
"["					{return new Symbol(sim_dpp.a_corchetes, yycolumn,yyline,new String(yytext()));}
"]"					{return new Symbol(sim_dpp.c_corchetes, yycolumn,yyline,new String(yytext()));}
","					{return new Symbol(sim_dpp.coma, yycolumn,yyline,new String(yytext()));}
"."					{return new Symbol(sim_dpp.punto, yycolumn,yyline,new String(yytext()));}
"?"					{return new Symbol(sim_dpp.interrogacion, yycolumn,yyline,new String(yytext()));}
":"					{return new Symbol(sim_dpp.dospuntos, yycolumn,yyline,new String(yytext()));}

{identificador}		{return new Symbol(sim_dpp.identificador, yycolumn,yyline,new String(yytext()));}

{comentario}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#737373"));
}

{comentario_multi}
{
	Entorno.cambiarColorTexto(yychar, yytext().length(), Color.decode("#737373"));
}

[ \t\r\f\n]+		{}

.
{
	RegistroError err = new RegistroError();
    err.setArchivo(Entorno.getArchivo());
    err.setFila(yyline);
    err.setColumna(yycolumn);
    err.setTipo("Lexico");
    err.setDescripcion("No se reconoce el simbolo " + yytext() + " .");
    Entorno.addError(err);
}
