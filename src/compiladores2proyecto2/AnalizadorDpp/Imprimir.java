/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class Imprimir {
    
    public static void generarImprimir(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        Dato dato = Expresiones.nodoExpresion(nodo.getNodo(0), metodo, ambito);
        
        if(dato.getTipo().equalsIgnoreCase("nulo"))
        {
            System.out.print("Dato impresion nulo.");
            return;
        }
        
        String tipo = dato.getTipo();
        
        escribir(dato.getValor());
        
        if(dato.getTipo().equalsIgnoreCase("booleano"))
        {
            String etqSalida = genEtq();

            escribir(dato.getEtqV());
            escribir("1");
            escribir("Br " + etqSalida);
            escribir(dato.getEtqF());
            escribir("0");
            escribir(etqSalida);
        }
        
        escribir("//Parametro 1");
        escribir("Get_local 0");
        escribir(String.valueOf(metodo.getTamano() + 1));
        escribir("Add");
        escribir("1");
        escribir("Add");
        escribir("Set_local $calc");

        escribir("//Cambio oficial");
        escribir("Get_local 0");
        escribir(String.valueOf(metodo.getTamano() + 1));
        escribir("Add");
        escribir("Set_local 0");
        
        switch(tipo)
        {
            case "entero":
            case "booleano":
                escribir("Call $printInt");
                break;
            case "decimal":
                escribir("Call $printDecimal");
                break;
            case "cadena":
                escribir("Call $printString");
                break;
            case "caracter":
                escribir("Call $printChar");
                break;
        }
        
        escribir("//Cambio oficial");
        escribir("Get_local 0");
        escribir(String.valueOf(metodo.getTamano() + 1));
        escribir("Diff");
        escribir("Set_local 0");
    }
}
