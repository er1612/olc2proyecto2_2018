/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class SentenciaSi {
    
    public static void generarSi(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        Nodo nodoExpresion = nodo.getNodo(0);
        Nodo nodoSentencias = nodo.getNodo(1);
        String etiquetaSalida = genEtq();
        
        escribir("//Sentencia SI");
        Dato dato = Expresiones.nodoExpresion(nodoExpresion, metodo, nodo.getAmbitoLocal());
        escribir(dato.getValor());
        
        escribir("//Etiqueta verdadera");
        escribir(dato.getEtqV());
        
        escribir("//Sentencias condicion verdadera");
        GenerarMetodo.generarCodigoSentencias(nodoSentencias, metodo, nodo.getAmbitoLocal());
        
        escribir("//Salto salida sentencia SI");
        escribir("Br " + etiquetaSalida);
        
        escribir("//Etiqueta condicion falsa");
        escribir(dato.getEtqF());
        
        Nodo nodoElse = null;
        Nodo nodoElseIf = null;
        
        for(Nodo sentencia : nodo.getNodos())
        {
            if(sentencia.getNombre().equalsIgnoreCase("sentencia_else"))
            {
                nodoElse = sentencia;
            }
            if(sentencia.getNombre().equalsIgnoreCase("sentencias_elif"))
            {
                nodoElseIf = sentencia;
            }
        }
        
        if(nodoElseIf!=null)
        {
            for(Nodo nodoElif : nodoElseIf.getNodos())
            {
                Nodo nodoExpresionElseif = nodoElif.getNodo(0);
                Nodo nodoSentenciasElseif = nodoElif.getNodo(1);
                
                escribir("//Sentencia SINO SI");
                Dato datoElseif = Expresiones.nodoExpresion(nodoExpresionElseif, metodo, nodoElif.getAmbitoLocal());
                escribir(datoElseif.getValor());
                
                escribir("//Etiqueta condicion verdadera");
                escribir(datoElseif.getEtqV());
                
                escribir("//Sentencias condicion verdadera");
                GenerarMetodo.generarCodigoSentencias(nodoSentenciasElseif, metodo, nodoElif.getAmbitoLocal());
                
                escribir("//Salto salida sentencia SI");
                escribir("Br " + etiquetaSalida);
                
                escribir("//Etiqueta condicion falsa");
                escribir(datoElseif.getEtqF());
            }
        }
        
        if(nodoElse != null)
        {
            escribir("//Sentencia SINO");
            GenerarMetodo.generarCodigoSentencias(nodoElse, metodo, nodoElse.getAmbitoLocal());
        }
        
        escribir("//Etiqueta de salida");
        escribir(etiquetaSalida);
    }
    
}
