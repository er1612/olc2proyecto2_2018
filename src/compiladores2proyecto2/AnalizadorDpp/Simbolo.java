/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class Simbolo {
    private String nombreAmbito;
    private String nombre;
    private String tipo;
    private int posicion;
    private int tamano;
    private String rol;
    private int dimensiones = 0;
    private Nodo nodoDimensiones = null;

    public String getNombreAmbito() {
        return nombreAmbito;
    }

    public void setNombreAmbito(String nombreAmbito) {
        this.nombreAmbito = nombreAmbito;
    } 

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getPosicion() {
        return posicion;
    }

    public void setPosicion(int posicion) {
        this.posicion = posicion;
    }

    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public int getDimensiones() {
        return dimensiones;
    }

    public void setDimensiones(int dimensiones) {
        this.dimensiones = dimensiones;
    }

    public Nodo getNodoDimensiones() {
        return nodoDimensiones;
    }

    public void setNodoDimensiones(Nodo nodoDimensiones) {
        this.nodoDimensiones = nodoDimensiones;
    }
    
    
}
