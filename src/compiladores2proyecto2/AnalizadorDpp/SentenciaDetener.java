/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class SentenciaDetener {
    
    public static void generarSentencia(Nodo nodo)
    {
        if(Display.getCiclos().isEmpty())
        {
            errorDetener(nodo);
            return;
        }
        
        if(Display.getCiclos().size()>0)
        {
            String etqSalida = Display.getLastCiclo().getEtiquetaSalida();
            escribir("Br " + etqSalida);
        }
    }
    
    public static void errorDetener(Nodo operador)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("La sentencia DETENER no esta dentro de un ciclo");
        Entorno.addError(error);
    }
    
}
