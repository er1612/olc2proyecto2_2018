/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import compiladores2proyecto2.Nodo;
import java.util.LinkedList;

/**
 *
 * @author rodolfo
 */
public class Metodo {
    private String nombre = "";
    private String tipo = "";
    private int tamano = 0;
    private Nodo sentencias = null;
    private Nodo parametros = null;
    private int cantParametros = 0;
    private Ambito ambitoGeneral = new Ambito();
    private Ambito ambitoPrincipal = new Ambito();

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public int getTamano() {
        return tamano;
    }

    public void setTamano(int tamano) {
        this.tamano = tamano;
    }

    public Nodo getParametros() {
        return parametros;
    }

    public void setParametros(Nodo parametros) {
        this.parametros = parametros;
    }

    
    public void aumentarTamano(int cantidad)
    {
        tamano += cantidad;
    }

    public Nodo getSentencias() {
        return sentencias;
    }

    public void setSentencias(Nodo sentencias) {
        this.sentencias = sentencias;
    }

    public int getCantParametros() {
        return cantParametros;
    }

    public void setCantParametros(int cantParametros) {
        this.cantParametros = cantParametros;
    }

    public Ambito getAmbitoGeneral() {
        return ambitoGeneral;
    }

    public void setAmbitoGeneral(Ambito ambitoGeneral) {
        this.ambitoGeneral = ambitoGeneral;
    }

    public Ambito getAmbitoPrincipal() {
        return ambitoPrincipal;
    }

    public void setAmbitoPrincipal(Ambito ambitoPrincipal) {
        this.ambitoPrincipal = ambitoPrincipal;
    }
    
    
    
}
