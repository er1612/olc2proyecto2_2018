/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class FuncionesDibujo {
    
    public static void generarPunto(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        escribir("//Generando Punto");
        
        Nodo nodoParametros = nodo.getNodo(0);
        
        Dato exp0 = Expresiones.nodoExpresion(nodoParametros.getNodo(0), metodo, ambito);
        escribir(exp0.getValor());
        
        Dato exp1 = Expresiones.nodoExpresion(nodoParametros.getNodo(1), metodo, ambito);
        escribir(exp1.getValor());
        
        String color = nodoParametros.getNodo(2).getToken().replace("#", "");
        String valor = color.trim();
        int hex = Integer.parseInt(valor, 16);
        escribir(String.valueOf(hex));
        
        Dato exp3 = Expresiones.nodoExpresion(nodoParametros.getNodo(3), metodo, ambito);
        escribir(exp3.getValor());
        
        escribir("Call $Point");
    }
    
    public static void generarCuadrado(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        escribir("//Generando Cuadrado");
        
        Nodo nodoParametros = nodo.getNodo(0);
        
        Dato exp0 = Expresiones.nodoExpresion(nodoParametros.getNodo(0), metodo, ambito);
        escribir(exp0.getValor());
        
        Dato exp1 = Expresiones.nodoExpresion(nodoParametros.getNodo(1), metodo, ambito);
        escribir(exp1.getValor());
        
        String color = nodoParametros.getNodo(2).getToken().replace("#", "");
        String valor = color.trim();
        int hex = Integer.parseInt(valor, 16);
        escribir(String.valueOf(hex));
        
        Dato exp3 = Expresiones.nodoExpresion(nodoParametros.getNodo(3), metodo, ambito);
        escribir(exp3.getValor());
        
        Dato exp4 = Expresiones.nodoExpresion(nodoParametros.getNodo(4), metodo, ambito);
        escribir(exp4.getValor());
        
        escribir("Call $Quadrate");
    }
    
    public static void generarOvalo(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        escribir("//Generando Ovalo");
        
        Nodo nodoParametros = nodo.getNodo(0);
        
        Dato exp0 = Expresiones.nodoExpresion(nodoParametros.getNodo(0), metodo, ambito);
        escribir(exp0.getValor());
        
        Dato exp1 = Expresiones.nodoExpresion(nodoParametros.getNodo(1), metodo, ambito);
        escribir(exp1.getValor());
        
        String color = nodoParametros.getNodo(2).getToken().replace("#", "");
        String valor = color.trim();
        int hex = Integer.parseInt(valor, 16);
        escribir(String.valueOf(hex));
        
        Dato exp3 = Expresiones.nodoExpresion(nodoParametros.getNodo(3), metodo, ambito);
        escribir(exp3.getValor());
        
        Dato exp4 = Expresiones.nodoExpresion(nodoParametros.getNodo(4), metodo, ambito);
        escribir(exp4.getValor());
        
        escribir("Call $Oval");
    }
    
    public static void generarCadena(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        escribir("//Generando Cadena");
        
        Nodo nodoParametros = nodo.getNodo(0);
        
        Dato exp0 = Expresiones.nodoExpresion(nodoParametros.getNodo(0), metodo, ambito);
        escribir(exp0.getValor());
        
        Dato exp1 = Expresiones.nodoExpresion(nodoParametros.getNodo(1), metodo, ambito);
        escribir(exp1.getValor());
        
        String color = nodoParametros.getNodo(2).getToken().replace("#", "");
        String valor = color.trim();
        int hex = Integer.parseInt(valor, 16);
        escribir(String.valueOf(hex));
        
        Dato exp3 = Expresiones.nodoExpresion(nodoParametros.getNodo(3), metodo, ambito);
        escribir(exp3.getValor());
        
        escribir("Call $String");
    }
    
    public static void generarLinea(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        escribir("//Generando Linea");
        
        Nodo nodoParametros = nodo.getNodo(0);
        
        Dato exp0 = Expresiones.nodoExpresion(nodoParametros.getNodo(0), metodo, ambito);
        escribir(exp0.getValor());
        
        Dato exp1 = Expresiones.nodoExpresion(nodoParametros.getNodo(1), metodo, ambito);
        escribir(exp1.getValor());
        
        Dato exp2 = Expresiones.nodoExpresion(nodoParametros.getNodo(2), metodo, ambito);
        escribir(exp2.getValor());
        
        Dato exp3 = Expresiones.nodoExpresion(nodoParametros.getNodo(3), metodo, ambito);
        escribir(exp3.getValor());
        
        String color = nodoParametros.getNodo(4).getToken().replace("#", "");
        String valor = color.trim();
        int hex = Integer.parseInt(valor, 16);
        escribir(String.valueOf(hex));
        
        Dato exp5 = Expresiones.nodoExpresion(nodoParametros.getNodo(5), metodo, ambito);
        escribir(exp5.getValor());
        
        escribir("Call $Line");
    }
    
}
