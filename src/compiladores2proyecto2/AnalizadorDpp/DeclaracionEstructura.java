/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class DeclaracionEstructura {
    
    public static void generarEstructura(Nodo nodo)
    {
        Metodo estructura = new Metodo();
        estructura.setNombre(nodo.getToken());
        estructura.setTipo(nodo.getToken());
        
        for(Nodo miembro : nodo.getNodos())
        {
            String nombre = miembro.getNombre();
            
            switch(nombre)
            {
                case "declaracion_variable":
                    agregarVariables(miembro, estructura);
                    break;
                case "arreglo":
                    agregarArreglo(miembro, estructura);
                    break;
            }
        }
        
        generarConstructor(estructura);
        EjecutarDpp.estructuras.add(estructura);
    }
    
    private static void agregarVariables(Nodo nodo, Metodo estructura)
    {
        String tipo = nodo.getTipo();
        
        for(Nodo miembro : nodo.getNodos())
        {
            String id = miembro.getToken();
            Simbolo sim = new Simbolo();
            sim.setDimensiones(0);
            sim.setNombre(id);
            sim.setNombreAmbito(estructura.getNombre());
            sim.setPosicion(estructura.getTamano());
            estructura.aumentarTamano(1);
            sim.setRol("variable");
            sim.setTamano(1);
            sim.setTipo(tipo);
            
            estructura.getAmbitoGeneral().addSimbolo(sim);
            estructura.getAmbitoPrincipal().addSimbolo(sim);
        }
    }
    
    private static void agregarArreglo(Nodo miembro, Metodo estructura)
    {
        String tipo = miembro.getTipo();
        
        int dimensiones = miembro.getNodo(0).getNodos().size();
        
        String id = miembro.getToken();
        Simbolo sim = new Simbolo();
        sim.setDimensiones(dimensiones);
        sim.setNombre(id);
        sim.setNombreAmbito(estructura.getNombre());
        sim.setPosicion(estructura.getTamano());
        estructura.aumentarTamano(1);
        sim.setRol("arreglo");
        sim.setTamano(1);
        sim.setTipo(tipo);
        
        sim.setNodoDimensiones(miembro.getNodo(0));

        estructura.getAmbitoGeneral().addSimbolo(sim);
        estructura.getAmbitoPrincipal().addSimbolo(sim);
    }
    
    private static String cons = "";
    
    private static void concat(String texto)
    {
        cons += texto + "\n";
    }
    
    private static void generarConstructor(Metodo estructura)
    {
        concat("");
        concat("Function $" + estructura.getNombre());
        
        concat("//Posicion inicial de la estructura");
        concat("Get_global 0");
        concat("Get_local 0");
        concat("Set_local $calc");
        
        concat("//Reservando el tamano de la estructura");
        concat("Get_global 0");
        concat(String.valueOf(estructura.getTamano()));
        concat("Add");
        concat("Set_global 0");
        
        Ambito ambito = estructura.getAmbitoPrincipal();
        
        for(Simbolo sim : ambito.getSimbolos())
        {
            concat("//" + sim.getNombre() + " - " + sim.getTipo());
            
            if(sim.getRol().equalsIgnoreCase("variable"))
            {
                if(sim.getTipo().equals("cadena"))
                {
                    concat("-1");
                    
                }
                else
                {
                    concat("0");
                }
                
                concat("Get_local 0");
                concat("Get_local $calc");
                concat(String.valueOf(sim.getPosicion()));
                concat("Add");
                concat("Set_global $calc");
            }
            else if(sim.getRol().equalsIgnoreCase("arreglo"))
            {
                Nodo dimensiones = sim.getNodoDimensiones();
                int cantidad = dimensiones.getNodos().size();
                int posicion = sim.getPosicion();

                for(int x=0; x<cantidad; x++)
                {
                    concat("//Dimension " + x);
                    Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), null, null);
                    concat(tamano.getValor());

                    concat("Get_global 0");
                    concat(String.valueOf(x));
                    concat("Add");
                    concat("Set_global $calc");
                }

                concat("//Asignar arreglo");

                concat("Get_global 0");
                
                concat("Get_local 0");
                concat("Get_local $calc");
                concat(String.valueOf(posicion));
                concat("Add");
                
                concat("Set_global $calc");

                concat("//Tamano lineal ");

                for(int x=0; x<cantidad; x++)
                {
                    concat("//Dimension " + x);
                    concat("Get_global 0");
                    concat(String.valueOf(x));
                    concat("Add");
                    concat("Get_global $calc");

                    if(x>0)
                    {
                        concat("Mult");
                    }
                }

                concat(String.valueOf(cantidad));
                concat("Add");
                concat("Get_global 0");
                concat("Add");
                concat("Set_global 0");
            }
        }
        
        concat("End");
        concat("");
        EjecutarDpp.codigoConstructores += cons;
        cons = "";
    }
}
