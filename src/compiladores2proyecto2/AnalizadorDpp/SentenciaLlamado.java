/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class SentenciaLlamado {
    
    public static void generarSentencia(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String idMetodo = nodo.getToken();
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        int tamanoMetodo = metodo.getTamano() + 1;

        for(Nodo parametro : nodo.getNodo(0).getNodos())
        {
            Dato param = Expresiones.nodoExpresion(parametro, metodo, ambito);
            idMetodo += "_" + param.getTipo();
            escribir(param.getValor());
        }

        for(int x = cantidadParametros; x>0; x--)
        {
            escribir("//Parametro " + String.valueOf(x));
            escribir("Get_local 0");
            escribir(String.valueOf(tamanoMetodo));
            escribir("Add");
            escribir(String.valueOf(x));
            escribir("Add");
            escribir("Set_local $calc");
        }

        escribir("//Cambio oficial");
        escribir("Get_local 0");
        escribir(String.valueOf(tamanoMetodo));
        escribir("Add");
        escribir("Set_local 0");
        escribir("call $" + idMetodo);
        escribir("//Cambio oficial");
        escribir("Get_local 0");
        escribir(String.valueOf(tamanoMetodo));
        escribir("Diff");
        escribir("Set_local 0");
    }
}
