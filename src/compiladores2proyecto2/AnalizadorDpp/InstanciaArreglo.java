/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.Asignacion.getVariable;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class InstanciaArreglo {
    
    public static void generarInstancia(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String id = nodo.getToken();
        String tipo = nodo.getToken();
        Nodo dimensiones = nodo.getNodo(0);
        
        Simbolo simbolo = getVariable(id, ambito);
        
        int cantidad = dimensiones.getNodos().size();
        int posicion = simbolo.getPosicion();
        
        for(int x=0; x<cantidad; x++)
        {
            escribir("//Dimension " + x);
            Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
            escribir(tamano.getValor());
            
            escribir("Get_global 0");
            escribir(String.valueOf(x));
            escribir("Add");
            escribir("Set_global $calc");
        }
        
        escribir("//Asignar arreglo");
        
        escribir("Get_global 0");
        escribir("Get_local 0");
        escribir(String.valueOf(posicion));
        escribir("Add");
        escribir("Set_local $calc");
        
        if(nodo.getNodos().size()>1)
        {
            escribir("//Asignar arreglo por defecto");
            nodoExp = new Nodo();
            nodoArreglo(nodo.getNodo(1));
            
            for(int x=0 ; x<nodoExp.getNodos().size(); x++)
            {
                escribir("//Agregando valor " + x);
                Dato exp = Expresiones.nodoExpresion(nodoExp.getNodo(x), metodo, ambito);
                escribir(exp.getValor());
                escribir("Get_global 0");
                escribir(String.valueOf(cantidad));
                escribir("Add");
                escribir(String.valueOf(x));
                escribir("Add");
                escribir("Set_global $calc");
            }
        }
        
        escribir("//Tamano lineal ");
        
        for(int x=0; x<cantidad; x++)
        {
            escribir("//Dimension " + x);
            escribir("Get_global 0");
            escribir(String.valueOf(x));
            escribir("Add");
            escribir("Get_global $calc");
            
            if(x>0)
            {
                escribir("Mult");
            }
        }
        
        escribir(String.valueOf(cantidad));
        escribir("Add");
        escribir("Get_global 0");
        escribir("Add");
        escribir("Set_global 0");
    }
    
    private static Nodo nodoExp = null;
    
    private static void nodoArreglo(Nodo nodo)
    {
        for(Nodo elemento : nodo.getNodos())
        {
            if(elemento.getNombre().equalsIgnoreCase("expresiones"))
            {
                nodoExpresiones(elemento);
            }
            else if(elemento.getNombre().equalsIgnoreCase("arreglo"))
            {
                nodoArreglo(elemento);
            }
        }

    }
    
    private static void nodoExpresiones(Nodo nodo)
    {
        for(Nodo expresion : nodo.getNodos())
        {
            nodoExp.addNodo(expresion);
        }
    }
    
}
