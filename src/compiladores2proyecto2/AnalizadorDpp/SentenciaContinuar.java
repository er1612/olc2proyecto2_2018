/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class SentenciaContinuar {
    
    public static void generarSentencia(Nodo nodo)
    {
        if(Display.getCiclos().isEmpty())
        {
            errorContinuar(nodo);
            return;
        }
        
        if(Display.getCiclos().size()>0)
        {
            String etqInicio = Display.getLastCiclo().getEtiquetaInicio();
            escribir("Br " + etqInicio);
        }
        
    }
    
    public static void errorContinuar(Nodo operador)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("La sentencia CONTINUAR no esta dentro de un ciclo");
        Entorno.addError(error);
    }
}
