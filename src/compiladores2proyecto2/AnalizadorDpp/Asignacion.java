/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.ambitoGlobal;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Entorno;
import compiladores2proyecto2.Nodo;
import compiladores2proyecto2.RegistroError;

/**
 *
 * @author rodolfo
 */
public class Asignacion {
    
    public static void generarDeclaracionAsignacion(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        for(Nodo variable: nodo.getNodos())
        {
            String id = variable.getToken();
            Simbolo simbolo = getVariable(id, ambito);
            int posicion = simbolo.getPosicion();
            String tipo = nodo.getTipo();
            
            Dato dato = null;
            
            if(variable.getNodos().size()>0)
            {
                dato = Expresiones.nodoExpresion(variable.getNodo(0), metodo, ambito);
        
                if(dato.getTipo().equalsIgnoreCase("nulo"))
                {
                    errorTipo(nodo, tipo, dato.getTipo());
                    return;
                }

                if(!dato.getTipo().equalsIgnoreCase(tipo))
                {
                    errorTipo(nodo, tipo, dato.getTipo());
                }

                escribir(dato.getValor());
            }
            else
            {
                if(tipo.equals("entero")||tipo.equals("decimal")||tipo.equals("caracter")||tipo.equals("booleano"))
                {
                    escribir("0");
                }
                else
                {
                    escribir("-1");
                }
            }
            
            if(tipo.equalsIgnoreCase("booleano"))
            {
                if(dato!=null)
                {
                    String etqSalida = genEtq();

                    escribir(dato.getEtqV());
                    escribir("1");
                    escribir("Br " + etqSalida);
                    escribir(dato.getEtqF());
                    escribir("0");
                    escribir(etqSalida);
                }
                else
                {
                    String etqSalida = genEtq();
                    String etqV = genEtq();
                    String etqF = genEtq();

                    escribir(etqV);
                    escribir("1");
                    escribir("Br " + etqSalida);
                    escribir(etqF);
                    escribir("0");
                    escribir(etqSalida);
                }
            }
            
            escribir("//Asignando a " + id);
            escribir("Get_local 0");
            escribir(String.valueOf(posicion));
            escribir("Add");
            escribir("Set_local $calc");
        }
    }
    
    public static void generarAsignacion(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        Dato dato = Expresiones.nodoExpresion(nodo.getNodo(1), metodo, ambito);
        
        if(dato.getTipo().equalsIgnoreCase("nulo"))
        {
            errorTipo(nodo, "", dato.getTipo());
        }
        
        escribir(dato.getValor());
        
        if(dato.getTipo().equalsIgnoreCase("booleano"))
        {
            String etqSalida = genEtq();
            
            escribir(dato.getEtqV());
            escribir("1");
            escribir("Br " + etqSalida);
            escribir(dato.getEtqF());
            escribir("0");
            escribir(etqSalida);
        }
        
        String tipoAux = "";
        Nodo nodoAccesos = nodo.getNodo(0);
        int cantAccesos = nodoAccesos.getNodos().size();
        
        for(Nodo acceso : nodoAccesos.getNodos())
        {
            String nombreAcceso = acceso.getNombre();
            
            //Identificador
            if(nombreAcceso.equalsIgnoreCase("identificador"))
            {
                String id = acceso.getToken();

                if(cantAccesos == 1)
                {
                    Simbolo simbolo = getVariable(id, ambito);
                    
                    if(simbolo==null)
                    {
                        errorVariable(nodo, id);
                        return;
                    }
                    
                    int posicion = simbolo.getPosicion();
                    
                    escribir("//Asignando a " + id);
                    
                    if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                    {
                        escribir("Set_local " + posicion);
                    }
                    else
                    {
                        escribir("Get_local 0");
                        escribir(String.valueOf(posicion));
                        escribir("Add");
                        escribir("Set_local $calc");
                    }
                }
                else if(cantAccesos == 2)
                {
                    if(acceso.equals(nodoAccesos.getNodos().getFirst()))
                    {
                        Simbolo simbolo = getVariable(id, ambito);
                        
                        if(simbolo==null)
                        {
                            errorVariable(nodo, id);
                            return;
                        }
                        
                        int posicion = simbolo.getPosicion();
                        tipoAux = simbolo.getTipo();
                        
                        escribir("//Posicion inicial de " + id);
                        
                        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                        {
                            escribir("Get_local " + posicion);
                        }
                        else
                        {
                            escribir("Get_local 0");
                            escribir(String.valueOf(posicion));
                            escribir("Add");
                            escribir("Get_local $calc");
                        }
                    }
                    else
                    {
                        Simbolo simbolo = getAtributo(tipoAux, id);
                        
                        if(simbolo==null)
                        {
                            errorAtributo(nodo, tipoAux, id);
                            return;
                        }
                        
                        int posicion = simbolo.getPosicion();
                        
                        escribir("//Asignando a " + tipoAux + "." + id);
                        escribir(String.valueOf(posicion));
                        escribir("Add");
                        escribir("Set_global $calc");
                    }
                }
            }
            //Arreglo
            else if(nombreAcceso.equalsIgnoreCase("acceso_arreglo"))
            {
                String id = acceso.getToken();
                
                if(cantAccesos == 1)
                {
                    Simbolo simbolo = getVariable(id, ambito);
                    
                    if(simbolo==null)
                    {
                        errorVariable(nodo, id);
                        return;
                    }
                    
                    int posicion = simbolo.getPosicion();
                    int cantidad1 = simbolo.getDimensiones();
                    Nodo dimensiones = acceso.getNodo(0);
                    int cantidad2 = dimensiones.getNodos().size();
                    
                    escribir("//Asignando a " + id);
                    
                    if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                    {
                        escribir("Get_local " + posicion);
                    }
                    else
                    {
                        escribir("Get_local 0");
                        escribir(String.valueOf(posicion));
                        escribir("Add");
                        escribir("Get_local $calc");
                    }
                    
                    
                    escribir(String.valueOf(cantidad1));
                    escribir("Add");
                    
                    escribir("//Accediendo al arreglo");
                    
                    for(int x=0; x<cantidad2; x++)
                    {
                        if(x>0)
                        {
                            if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                            {
                                escribir("Get_local " + posicion);
                            }
                            else
                            {
                                escribir("Get_local 0");
                                escribir(String.valueOf(posicion));
                                escribir("Add");
                                escribir("Get_local $calc");
                            }
                            
                            escribir(String.valueOf(x));
                            escribir("Add");
                            escribir("Get_global $calc");
                            escribir("Mult");

                        }
                        
                        Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
                        escribir(tamano.getValor());
                        
                        if(x>0)
                        {
                            escribir("Add");
                        }
                    }
                    
                    escribir("Add");
                    escribir("Set_global $calc");
                    
                }
                else if(cantAccesos == 2)
                {
                    if(acceso.equals(nodoAccesos.getNodos().getFirst()))
                    {
                        Simbolo simbolo = getVariable(id, ambito);
                        
                        if(simbolo==null)
                        {
                            errorVariable(nodo, id);
                            return;
                        }
                    
                        int posicion = simbolo.getPosicion();
                        int cantidad1 = simbolo.getDimensiones();
                        Nodo dimensiones = acceso.getNodo(0);
                        int cantidad2 = dimensiones.getNodos().size();
                        tipoAux = simbolo.getTipo();

                        escribir("//Asignando a " + id);

                        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                        {
                            escribir("Get_local " + posicion);
                        }
                        else
                        {
                            escribir("Get_local 0");
                            escribir(String.valueOf(posicion));
                            escribir("Add");
                            escribir("Get_local $calc");
                        }


                        escribir(String.valueOf(cantidad1));
                        escribir("Add");

                        escribir("//Accediendo al arreglo");

                        for(int x=0; x<cantidad2; x++)
                        {
                            if(x>0)
                            {
                                if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                                {
                                    escribir("Get_local " + posicion);
                                }
                                else
                                {
                                    escribir("Get_local 0");
                                    escribir(String.valueOf(posicion));
                                    escribir("Add");
                                    escribir("Get_local $calc");
                                }

                                escribir(String.valueOf(x));
                                escribir("Add");
                                escribir("Get_global $calc");
                                escribir("Mult");

                            }

                            Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
                            escribir(tamano.getValor());

                            if(x>0)
                            {
                                escribir("Add");
                            }
                        }

                        escribir("Add");
                        escribir("Get_global $calc");
                    }
                    else
                    {
                        Simbolo simbolo = getAtributo(tipoAux, id);
                        
                        if(simbolo==null)
                        {
                            errorAtributo(nodo, tipoAux,  id);
                            return;
                        }
                    
                        int posicion = simbolo.getPosicion();
                        int cantidad1 = simbolo.getDimensiones();
                        Nodo dimensiones = acceso.getNodo(0);
                        int cantidad2 = dimensiones.getNodos().size();

                        escribir("//Asignando a " + id);
                        
                        escribir(String.valueOf(posicion));
                        escribir("Add");
                        escribir("Get_local 0");
                        escribir("Set_local $calc");
                        
                        escribir("Get_local 0");
                        escribir("Get_local $calc");
                        escribir("Get_global $calc");
                        
                        escribir(String.valueOf(cantidad1));
                        escribir("Add");

                        escribir("//Accediendo al arreglo");

                        for(int x=0; x<cantidad2; x++)
                        {
                            if(x>0)
                            {
                                escribir("Get_local 0");
                                escribir("Get_local $calc");
                                escribir("Get_global $calc");

                                escribir(String.valueOf(x));
                                escribir("Add");
                                escribir("Get_global $calc");
                                escribir("Mult");

                            }

                            Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
                            escribir(tamano.getValor());

                            if(x>0)
                            {
                                escribir("Add");
                            }
                        }

                        escribir("Add");
                        escribir("Set_global $calc");
                    }
                }
            }
            else if(nombreAcceso.equalsIgnoreCase("llamado"))
            {
                String idMetodo = acceso.getToken();
                int cantidadParametros = acceso.getNodo(0).getNodos().size();
                int tamanoMetodo = metodo.getTamano() + 1;

                for(Nodo parametro : acceso.getNodo(0).getNodos())
                {
                    Dato param = Expresiones.nodoExpresion(parametro, metodo, ambito);
                    idMetodo += "_" + param.getTipo();
                    escribir(param.getValor());
                }
                
                if(!metodoExiste(idMetodo))
                {
                    errorMetodo(nodo, idMetodo);
                }

                for(int x = cantidadParametros; x>0; x--)
                {
                    escribir("//Parametro " + String.valueOf(x));
                    escribir("Get_local 0");
                    escribir(String.valueOf(tamanoMetodo));
                    escribir("Add");
                    escribir(String.valueOf(x));
                    escribir("Add");
                    escribir("Set_local $calc");
                }

                escribir("//Cambio oficial");
                escribir("Get_local 0");
                escribir(String.valueOf(tamanoMetodo));
                escribir("Add");
                escribir("Set_local 0");
                escribir("call $" + idMetodo);
                escribir("//Obteniendo retorno");
                escribir("Get_local 0");
                escribir("Get_local $calc");
                escribir("//Cambio oficial");
                escribir("Get_local 0");
                escribir(String.valueOf(tamanoMetodo));
                escribir("Diff");
                escribir("Set_local 0");

                for(Metodo met : EjecutarDpp.metodos)
                {
                    if(met.getNombre().equals(idMetodo))
                    {
                        tipoAux = met.getTipo();
                    }
                }
            }
        }
    }
    
    public static Simbolo getVariable(String id, Ambito ambito)
    {

        for(Simbolo simbolo : ambito.getSimbolos())
        {
            if(simbolo.getNombre().equals(id))
            {
                return simbolo;
            }
        }
        
        for(Simbolo simbolo : ambitoGlobal.getAmbitoPrincipal().getSimbolos())
        {
            if(simbolo.getNombre().equals(id))
            {
                return simbolo;
            }
        }

        return null;
    }
    
    public static Simbolo getAtributo(String id, String atributo)
    {
        for(Metodo estructura : EjecutarDpp.estructuras)
        {
            if(estructura.getNombre().equals(id))
            {
                for(Simbolo sim : estructura.getAmbitoPrincipal().getSimbolos())
                {
                    if(sim.getNombre().equals(atributo))
                    {
                        return sim;
                    }
                }
            }
        }
        return null;
    }
    
    public static void errorVariable(Nodo operador, String id)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se encontro la variable " + id + ".");
        Entorno.addError(error);
    }
    
    public static void errorAtributo(Nodo operador, String objeto, String id)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se encontro el atributo " + objeto + " - " + id + ".");
        Entorno.addError(error);
    }
    
    public static void errorTipo(Nodo operador, String tipo1, String tipo2)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("Se intenta asignar a una variable " + tipo1 + " un valor tipo " + tipo2 + ".");
        Entorno.addError(error);
    }
    
    public static void errorMetodo(Nodo operador, String id)
    {
        RegistroError error = new RegistroError();
        error.setArchivo(operador.getArchivo());
        error.setFila(operador.getFila());
        error.setColumna(operador.getColumna());
        error.setTipo("Semantico");
        error.setDescripcion("No se encontro el metodo " + id + ".");
        Entorno.addError(error);
    }
    
    public static boolean metodoExiste(String id)
    {
        for(Metodo metodo : EjecutarDpp.metodos)
        {
            if(metodo.getNombre().equals(id))
            {
                return true;
            }
        }
        return false;
    }
}
