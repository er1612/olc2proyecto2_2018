/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.Asignacion.errorAtributo;
import static compiladores2proyecto2.AnalizadorDpp.Asignacion.errorMetodo;
import static compiladores2proyecto2.AnalizadorDpp.Asignacion.errorVariable;
import static compiladores2proyecto2.AnalizadorDpp.Asignacion.getAtributo;
import static compiladores2proyecto2.AnalizadorDpp.Asignacion.getVariable;
import static compiladores2proyecto2.AnalizadorDpp.Asignacion.metodoExiste;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import static compiladores2proyecto2.AnalizadorDracoScript.Expresiones.getDatoNulo;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class ExpresionAcceso {
    
    public static Dato ejecutarAcceso(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        Dato dato = new Dato();
        String tipoAux = "";
        int cantAccesos = nodo.getNodos().size();
        
        for(Nodo acceso : nodo.getNodos())
        {
            String nombreAcceso = acceso.getNombre();
            
            //Identificador
            if(nombreAcceso.equalsIgnoreCase("identificador"))
            {
                String id = acceso.getToken();

                if(cantAccesos == 1)
                {
                    Simbolo simbolo = getVariable(id, ambito);
                    
                    if(simbolo==null)
                    {
                        errorVariable(nodo, id);
                        return getDatoNulo();
                    }
                    
                    int posicion = simbolo.getPosicion();
                    dato.setTipo(simbolo.getTipo());
                    
                    if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                    {
                        dato.concat("Get_local " + posicion);
                    }
                    else
                    {
                        dato.concat("Get_local 0");
                        dato.concat(String.valueOf(posicion));
                        dato.concat("Add");
                        dato.concat("Get_local $calc");
                    }
                    
                    return dato;
                }
                else if(cantAccesos == 2)
                {
                    if(acceso.equals(nodo.getNodos().getFirst()))
                    {
                        Simbolo simbolo = getVariable(id, ambito);
                        
                        if(simbolo==null)
                        {
                            errorVariable(nodo, id);
                            return getDatoNulo();
                        }

                        int posicion = simbolo.getPosicion();
                        tipoAux = simbolo.getTipo();
                        
                        escribir("//Posicion inicial de " + id);
                        
                        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                        {
                            dato.concat("Get_local " + posicion);
                        }
                        else
                        {
                            dato.concat("Get_local 0");
                            dato.concat(String.valueOf(posicion));
                            dato.concat("Add");
                            dato.concat("Get_local $calc");
                        }
                    }
                    else
                    {
                        Simbolo simbolo = getAtributo(tipoAux, id);
                        
                        if(simbolo==null)
                        {
                            errorAtributo(nodo, tipoAux,  id);
                            return getDatoNulo();
                        }
                        
                        int posicion = simbolo.getPosicion();
                        dato.setTipo(simbolo.getTipo());
                        
                        dato.concat(String.valueOf(posicion));
                        dato.concat("Add");
                        dato.concat("Get_global $calc");
                        
                        return dato;
                    }
                }
            }
            //Arreglo
            else if(nombreAcceso.equalsIgnoreCase("acceso_arreglo"))
            {
                String id = acceso.getToken();
                
                if(cantAccesos == 1)
                {
                    Simbolo simbolo = getVariable(id, ambito);
                    
                    if(simbolo==null)
                    {
                        errorVariable(nodo, id);
                        return getDatoNulo();
                    }
                    
                    int posicion = simbolo.getPosicion();
                    int cantidad1 = simbolo.getDimensiones();
                    Nodo dimensiones = acceso.getNodo(0);
                    int cantidad2 = dimensiones.getNodos().size();
                    dato.setTipo(simbolo.getTipo());
                    
                    dato.concat("//Asignando a " + id);
                    
                    if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                    {
                        dato.concat("Get_local " + posicion);
                    }
                    else
                    {
                        dato.concat("Get_local 0");
                        dato.concat(String.valueOf(posicion));
                        dato.concat("Add");
                        dato.concat("Get_local $calc");
                    }
                    
                    
                    dato.concat(String.valueOf(cantidad1));
                    dato.concat("Add");
                    
                    dato.concat("//Accediendo al arreglo");
                    
                    for(int x=0; x<cantidad2; x++)
                    {
                        if(x>0)
                        {
                            if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                            {
                                dato.concat("Get_local " + posicion);
                            }
                            else
                            {
                                dato.concat("Get_local 0");
                                dato.concat(String.valueOf(posicion));
                                dato.concat("Add");
                                dato.concat("Get_local $calc");
                            }
                            
                            dato.concat(String.valueOf(x));
                            dato.concat("Add");
                            dato.concat("Get_global $calc");
                            dato.concat("Mult");

                        }
                        
                        Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
                        dato.concat(tamano.getValor());
                        
                        if(x>0)
                        {
                            dato.concat("Add");
                        }
                    }
                    
                    dato.concat("Add");
                    dato.concat("Get_global $calc");
                    return dato;
                }
                else if(cantAccesos == 2)
                {
                    if(acceso.equals(nodo.getNodos().getFirst()))
                    {
                        Simbolo simbolo = getVariable(id, ambito);
                        
                        if(simbolo==null)
                        {
                            errorVariable(nodo, id);
                            return getDatoNulo();
                        }
                    
                        int posicion = simbolo.getPosicion();
                        int cantidad1 = simbolo.getDimensiones();
                        Nodo dimensiones = acceso.getNodo(0);
                        int cantidad2 = dimensiones.getNodos().size();
                        tipoAux = simbolo.getTipo();

                        dato.concat("//Asignando a " + id);

                        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                        {
                            dato.concat("Get_local " + posicion);
                        }
                        else
                        {
                            dato.concat("Get_local 0");
                            dato.concat(String.valueOf(posicion));
                            dato.concat("Add");
                            dato.concat("Get_local $calc");
                        }


                        dato.concat(String.valueOf(cantidad1));
                        dato.concat("Add");

                        dato.concat("//Accediendo al arreglo");

                        for(int x=0; x<cantidad2; x++)
                        {
                            if(x>0)
                            {
                                if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
                                {
                                    dato.concat("Get_local " + posicion);
                                }
                                else
                                {
                                    dato.concat("Get_local 0");
                                    dato.concat(String.valueOf(posicion));
                                    dato.concat("Add");
                                    dato.concat("Get_local $calc");
                                }

                                dato.concat(String.valueOf(x));
                                dato.concat("Add");
                                dato.concat("Get_global $calc");
                                dato.concat("Mult");

                            }

                            Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
                            dato.concat(tamano.getValor());

                            if(x>0)
                            {
                                dato.concat("Add");
                            }
                        }

                        dato.concat("Add");
                        dato.concat("Get_global $calc");
                    }
                    else
                    {
                        Simbolo simbolo = getAtributo(tipoAux, id);
                        
                        if(simbolo==null)
                        {
                            errorAtributo(nodo, tipoAux, id);
                            return getDatoNulo();
                        }
                    
                        int posicion = simbolo.getPosicion();
                        int cantidad1 = simbolo.getDimensiones();
                        Nodo dimensiones = acceso.getNodo(0);
                        int cantidad2 = dimensiones.getNodos().size();
                        dato.setTipo(simbolo.getTipo());

                        dato.concat("//Asignando a " + id);
                        
                        dato.concat(String.valueOf(posicion));
                        dato.concat("Add");
                        dato.concat("Get_local 0");
                        dato.concat("Set_local $calc");
                        
                        dato.concat("Get_local 0");
                        dato.concat("Get_local $calc");
                        dato.concat("Get_global $calc");
                        
                        dato.concat(String.valueOf(cantidad1));
                        dato.concat("Add");

                        dato.concat("//Accediendo al arreglo");

                        for(int x=0; x<cantidad2; x++)
                        {
                            if(x>0)
                            {
                                dato.concat("Get_local 0");
                                dato.concat("Get_local $calc");
                                dato.concat("Get_global $calc");

                                dato.concat(String.valueOf(x));
                                dato.concat("Add");
                                dato.concat("Get_global $calc");
                                dato.concat("Mult");

                            }

                            Dato tamano = Expresiones.nodoExpresion(dimensiones.getNodo(x), metodo, ambito);
                            dato.concat(tamano.getValor());

                            if(x>0)
                            {
                                dato.concat("Add");
                            }
                        }

                        dato.concat("Add");
                        dato.concat("Get_global $calc");
                        return dato;
                    }
                }
            }
            //Llamado
            else if(nombreAcceso.equalsIgnoreCase("llamado"))
            {
                String idMetodo = acceso.getToken();
                int cantidadParametros = acceso.getNodo(0).getNodos().size();
                int tamanoMetodo = metodo.getTamano() + 1;

                for(Nodo parametro : acceso.getNodo(0).getNodos())
                {
                    Dato param = Expresiones.nodoExpresion(parametro, metodo, ambito);
                    idMetodo += "_" + param.getTipo();
                    dato.concat(param.getValor());
                }
                
                if(!metodoExiste(idMetodo))
                {
                    errorMetodo(nodo, idMetodo);
                }

                for(int x = cantidadParametros; x>0; x--)
                {
                    dato.concat("//Parametro " + String.valueOf(x));
                    dato.concat("Get_local 0");
                    dato.concat(String.valueOf(tamanoMetodo));
                    dato.concat("Add");
                    dato.concat(String.valueOf(x));
                    dato.concat("Add");
                    dato.concat("Set_local $calc");
                }

                dato.concat("//Cambio oficial");
                dato.concat("Get_local 0");
                dato.concat(String.valueOf(tamanoMetodo));
                dato.concat("Add");
                dato.concat("Set_local 0");
                dato.concat("call $" + idMetodo);
                dato.concat("//Obteniendo retorno");
                dato.concat("Get_local 0");
                dato.concat("Get_local $calc");
                dato.concat("//Cambio oficial");
                dato.concat("Get_local 0");
                dato.concat(String.valueOf(tamanoMetodo));
                dato.concat("Diff");
                dato.concat("Set_local 0");

                for(Metodo met : EjecutarDpp.metodos)
                {
                    if(met.getNombre().equals(idMetodo))
                    {
                        dato.setTipo(met.getTipo());
                        return dato;
                    }
                }
            }
        }

        return getDatoNulo();
    }
    
    public static Dato ejecutarAccesoxxx(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String tipoAcceso = "";
        
        for(Nodo acceso : nodo.getNodos())
        {
            tipoAcceso = acceso.getNombre();
            
            switch(tipoAcceso)
            {
                case "identificador":
                    return accesoIdentificador(acceso, metodo, ambito);
                case "llamado":
                    return accesoLlamado(acceso, metodo, ambito);
                case "acceso_arreglo":
                    break;
            }
        }
        
        return getDatoNulo();
    }
    
    public static Dato accesoIdentificador(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String id = nodo.getToken();
        Simbolo simbolo = getVariable(id, ambito);
        int posicion = simbolo.getPosicion();
        String tipo = simbolo.getTipo();
        Dato dato = new Dato();
        dato.setTipo(tipo);

        if(simbolo.getNombreAmbito().equalsIgnoreCase("global"))
        {
            dato.concat("//Valor de la variable global " + id);
            dato.concat("Get_local " + simbolo.getPosicion());
        }
        else
        {
            dato.concat("//Valor de la variable " + id);
            dato.concat("Get_local 0");
            dato.concat(String.valueOf(posicion));
            dato.concat("Add");
            dato.concat("Get_local $calc");
        }

        return dato;
    }
    
    public static Dato accesoLlamado(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String idMetodo = nodo.getToken();
        int cantidadParametros = nodo.getNodo(0).getNodos().size();
        int tamanoMetodo = metodo.getTamano() + 1;
        Dato dato = new Dato();
        
        for(Nodo parametro : nodo.getNodo(0).getNodos())
        {
            Dato param = Expresiones.nodoExpresion(parametro, metodo, ambito);
            idMetodo += "_" + param.getTipo();
            dato.concat(param.getValor());
        }
        
        for(int x = cantidadParametros; x>0; x--)
        {
            dato.concat("//Parametro " + String.valueOf(x));
            dato.concat("Get_local 0");
            dato.concat(String.valueOf(tamanoMetodo));
            dato.concat("Add");
            dato.concat(String.valueOf(x));
            dato.concat("Add");
            dato.concat("Set_local $calc");
        }
        
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(tamanoMetodo));
        dato.concat("Add");
        dato.concat("Set_local 0");
        dato.concat("call $" + idMetodo);
        dato.concat("//Obteniendo retorno");
        dato.concat("Get_local 0");
        dato.concat("Get_local $calc");
        dato.concat("//Cambio oficial");
        dato.concat("Get_local 0");
        dato.concat(String.valueOf(tamanoMetodo));
        dato.concat("Diff");
        dato.concat("Set_local 0");
        
        for(Metodo met : EjecutarDpp.metodos)
        {
            if(met.getNombre().equals(idMetodo))
            {
                dato.setTipo(met.getTipo());
                return dato;
            }
        }
        
        return getDatoNulo();
    }
    
}
