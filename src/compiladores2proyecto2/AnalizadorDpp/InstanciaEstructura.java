/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.Asignacion.getVariable;
import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.escribir;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class InstanciaEstructura {
    
    public static void generarInstancia(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        String id = nodo.getToken();
        Simbolo simbolo = getVariable(id, ambito);
        int posicion = simbolo.getPosicion();
        
        escribir("//Cambio oficial");
        escribir("Get_local 0");
        escribir(String.valueOf(metodo.getTamano() + 1));
        escribir("Add");
        escribir("Set_local 0");
        escribir("Call $" + nodo.getTipo());
        escribir("//Obteniendo valor de retorno");
        escribir("Get_local 0");
        escribir("Get_local $calc");
        escribir("//Cambio oficial");
        escribir("Get_local 0");
        escribir(String.valueOf(metodo.getTamano() + 1));
        escribir("Diff");
        escribir("Set_local 0");
        
        escribir("//Asignando a " + id);
        escribir("Get_local 0");
        escribir(String.valueOf(posicion));
        escribir("Add");
        escribir("Set_local $calc");
    }
    
}
