/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2.AnalizadorDpp;

import static compiladores2proyecto2.AnalizadorDpp.EjecutarDpp.genEtq;
import static compiladores2proyecto2.AnalizadorDpp.Expresiones.getDatoNulo;
import compiladores2proyecto2.AnalizadorDracoScript.Dato;
import compiladores2proyecto2.Nodo;

/**
 *
 * @author rodolfo
 */
public class SentenciaSeleccion {
    
    public static Dato generarSentencia(Nodo nodo, Metodo metodo, Ambito ambito)
    {
        Dato nuevoDato = new Dato();
        String etiquetaSalida = genEtq();
        
        nuevoDato.concat("//Sentencia SELECCION");
        Dato condicion = Expresiones.nodoExpresion(nodo.getNodo(0), metodo, ambito);
        nuevoDato.concat(condicion.getValor());
        
        nuevoDato.concat("//Etiqueta verdadera");
        nuevoDato.concat(condicion.getEtqV());
        
        nuevoDato.concat("//Sentencias condicion verdadera");
        Dato expV = Expresiones.nodoExpresion(nodo.getNodo(1), metodo, ambito);
        nuevoDato.concat(expV.getValor());
        
        nuevoDato.concat("//Salto salida sentencia SI");
        nuevoDato.concat("Br " + etiquetaSalida);
        
        nuevoDato.concat("//Etiqueta condicion falsa");
        nuevoDato.concat(condicion.getEtqF());
        Dato expF = Expresiones.nodoExpresion(nodo.getNodo(2), metodo, ambito);
        nuevoDato.concat(expF.getValor());
        
        nuevoDato.concat("//Etiqueta de salida");
        nuevoDato.concat(etiquetaSalida);
        
        nuevoDato.setTipo(expV.getTipo());
        
        return nuevoDato;
    }
    
}
