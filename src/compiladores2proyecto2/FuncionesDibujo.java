/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package compiladores2proyecto2;

import compiladores2proyecto2.AnalizadorDracoScript.EjecutarDraco;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

/**
 *
 * @author rodolfo
 */
public class FuncionesDibujo {
    
    public static void ejecutarFuncionDibujo(java.awt.Graphics g)
    {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 500, 500);


        for(Dibujo dibujo : EjecutarDraco.dibujos)
        {
            switch(dibujo.getForma())
            {
                case "punto":
                    dibujarPunto(g, dibujo.getX(), dibujo.getY(), dibujo.getColor(), dibujo.getAncho());
                    break;
                case "rectangulo":
                    dibujarRectangulo(g, dibujo.getX(), dibujo.getY(), dibujo.getColor(), dibujo.getAncho(), 
                            dibujo.getAlto());
                    break;
                case "circulo":
                    dibujarCirculo(g, dibujo.getX(), dibujo.getY(), dibujo.getColor(), dibujo.getAncho(),
                            dibujo.getAlto());
                    break;
                case "cadena":
                    dibujarCadena(g, dibujo.getX(), dibujo.getY(), dibujo.getColor(), dibujo.getTexto());
                    break;
                case "linea":
                    dibujarLinea(g, dibujo.getX(), dibujo.getY(), dibujo.getXf(), dibujo.getYf(),
                            dibujo.getColor(), dibujo.getAncho());
                    break;
            }
        }
    }
    
    
    public static void dibujarPunto(java.awt.Graphics g, int x, int y, String color, int diametro)
    {
        g.setColor(Color.decode(color));
        g.fillOval(x, y, diametro, diametro);
    }
    
    public static void dibujarRectangulo(java.awt.Graphics g, int x, int y, String color, int ancho, int alto)
    {
        g.setColor(Color.decode(color));
        g.fillRect(x, y, ancho, alto);
    }
    
    public static void dibujarCirculo(java.awt.Graphics g, int x, int y, String color, int ancho, int alto)
    {
        g.setColor(Color.decode(color));
        g.fillOval(x, y, ancho, alto);
    }
    
    public static void dibujarCadena(java.awt.Graphics g, int x, int y, String color, String cadena)
    {
        Font fuente=new Font("Monospaced", Font.ITALIC, 16);
        g.setFont(fuente);
        g.setColor(Color.decode(color));		
        g.drawString(cadena, x, y);
    }
    
    public static void dibujarLinea(java.awt.Graphics g, int x, int y, int xf, int yf, String color, int ancho)
    {
        Graphics2D g2d = (Graphics2D)g;
        g2d.setColor(Color.decode(color));
        g2d.setStroke(new BasicStroke(ancho));
	g2d.drawLine(x, y, xf, yf);
    }
    
}
