Function $Principal
	1
	Set_local 0
	25.51
	Set_local 2
	1
	Set_local 3
	1
	Set_global 0
	
	$L3
	
	Get_local 0
	1
	Add
	Get_local $calc

	Get_local 0
	2
	Add
	Get_local $calc
	10
	Mult

	Gte
	Br_if $L4

	Get_local 0
	2
	Add
	Get_local $calc
	10
	Mult
	
	Get_local 0
	2
	Add
	Set_local $calc
	
	Br $L3

	$L4

	Get_local 0
	1
	Add
	Get_local $calc

	Get_local 0
	1
	Add
	Get_local $calc

	Get_local 0
	2
	Add
	Get_local $calc

	Mod
	Diff

	Get_local 0
	2
	Add
	Get_local $calc

	Div
	48
	Add

	Get_global 0
	Set_global $calc

	Get_global 0
	1
	Add
	Set_global 0

	//Comprobando si la posicion de
	//unidades es 1
	Get_local 0
	2
	Add
	Get_local $calc

	1
	Diff
	Br_if $L5

	//Actualizando el valor del entero
	Get_local 0
	1
	Add
	Get_local $calc

	Get_local 0
	2
	Add
	Get_local $calc

	Mod

	Get_local 0
	1
	Add
	Set_local $calc

	//Actualizando la posicion de unidades
	Get_local 0
	2
	Add
	Get_local $calc
	10
	Div
	
	Get_local 0
	2
	Add
	Set_local $calc

	//Regresando al ciclo
	Br $L4

	//Ciclo de salida
	$L5
	
	//Escribiendo el punto decimal
	46
	Get_global 0
	Set_Global $calc

	Get_global 0
	1
	Add
	Set_global 0

	//Guardando la cantidad de decimales
	5
	Get_local 0
	2
	Add
	Set_local $calc

	$L6

	//Separando la parte decimal
	Get_local 0
	1
	Add
	Get_local $calc

	1
	Mod

	//Guardando la parte decimal
	Get_local 0
	1
	Add
	Set_local $calc

	//Multiplicando la parte decimal por 10
	Get_local 0
	1
	Add
	Get_local $calc

	10
	Mult

	//Obteniendo el valor y su residuo
	Get_local 0
	1
	Add
	Get_local $calc
	
	10
	Mult

	1
	Mod
	
	//Restando la parte decimal del entero
	Diff

	//Sumando para obtener el ASCII
	48
	Add

	//Asignando valor al heap
	Get_global 0
	Set_global $calc

	Get_global 0
	1
	Add
	Set_global 0

	//Multiplicando la parte decimal por 10
	Get_local 0
	1
	Add
	Get_local $calc

	10
	Mult

	Get_local 0
	1
	Add
	Set_local $calc

	//Actualizando contador
	Get_local 0
	2
	Add
	Get_local $calc

	1
	Diff

	Get_local 0
	2
	Add
	Set_local $calc

	Get_local 0
	2
	Add
	Get_local $calc

	1
	Lt

	Br_if $L6

	//Agregando el final de cadena
	0
	Get_global 0
	Set_Global $calc

	Get_global 0
	1
	Add
	Set_global 0
	
End